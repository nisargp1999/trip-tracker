#include "Boat.h"

Boat::Boat()
{
	propellerLevel = 0;
	launch = false;
	dock = true;
}
int Boat::getPropellerLevel()
{
	return propellerLevel;
}
bool Boat::getLaunch()
{
	return launch;
}
bool Boat::getDock()
{
	return dock;
}
void Boat::setPropellerLevel(int thisLevel)
{
	propellerLevel = thisLevel;
}
void Boat::setLaunch(bool thisLaunch)
{
	launch = thisLaunch;
}
void Boat::setDock(bool thisDock)
{
	dock = thisDock;
}