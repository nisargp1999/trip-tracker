#pragma once
#ifndef BOAT_H
#define BOAT_H

#include "Vehicle.h"
#include <string>
#include <vector>
#include <iostream>
class Boat : public Vehicle
{
public:
	Boat();
	int getPropellerLevel();
	bool getLaunch();
	bool getDock();
	void setPropellerLevel(int thisLevel);
	void setLaunch(bool thisLaunch);
	void setDock(bool thisDock);
private:
	int propellerLevel;
	bool launch;
	bool dock;
};

#endif // !BOAT_H
