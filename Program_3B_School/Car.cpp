#include "Car.h"

Car::Car()
{
	raceCarStatus = false;
	sunRoofOpen = false;
	setPrice(15000);
}
void Car::setRaceCarStatus(bool thisStatus)
{
	raceCarStatus = thisStatus;
}
void Car::setSunRoofStatus(bool thisOpen)
{
	sunRoofOpen = thisOpen;
}
bool Car::getRaceCarStatus()
{
	return raceCarStatus;
}
bool Car::getSunroofOpen()
{
	return sunRoofOpen;
}