#pragma once
#ifndef CAR_H
#define CAR_H

#include "LandVehicle.h"
#include <string>
#include <vector>
#include <iostream>
class Car : public LandVehicle
{
public:
	Car();
	void setRaceCarStatus(bool thisStatus);
	void setSunRoofStatus(bool thisOpen);
	bool getRaceCarStatus();
	bool getSunroofOpen();
private:
	bool raceCarStatus;
	bool sunRoofOpen;
};


#endif // !CAR_H
