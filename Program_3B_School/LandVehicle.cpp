#include "LandVehicle.h"

LandVehicle::LandVehicle()
{
	mileage = 1000;
	coFriction = 0.5;
	applyFriction(getSpeed());
	airOn = false;
}
int LandVehicle::getMileage()
{
	return mileage;
}
float LandVehicle::getCoFriction()
{
	return coFriction;
}
float LandVehicle::getFriction()
{
	applyFriction(getSpeed());
	return friction;
}
bool LandVehicle::getAirOn()
{
	return airOn;
}
void LandVehicle::turnAirOn() 
{
	airOn = true;
}
void LandVehicle::turnAirOff() 
{
	airOn = false;
}
void LandVehicle::setMileage(int thisMileage) 
{
	mileage = thisMileage;
}
void LandVehicle::addMileage(int thisMileage) 
{
	mileage += thisMileage;
}
void LandVehicle::setCoFriction(float thisCoFriction) 
{
	coFriction = thisCoFriction;
	applyFriction(coFriction);
}
void LandVehicle::applyFriction(int thisSpeed) 
{
	friction = coFriction * thisSpeed;
}