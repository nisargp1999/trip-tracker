#ifndef LANDVEHICLE_H
#define LANDVEHICLE_H

#include "Vehicle.h"
using namespace std;

class LandVehicle : public Vehicle
{
public:
	LandVehicle();
	int getMileage();
	float getCoFriction();
	float getFriction();
	bool getAirOn();
	void turnAirOn();
	void turnAirOff();
	void setMileage(int thisMileage);
	void addMileage(int thisMileage);
	void setCoFriction(float thisCoFriction);
	void applyFriction(int thisSpeed);
private:
	int mileage;
	float coFriction;
	float friction;
	bool airOn;
};



#endif // !LANDVEHICLE_H
