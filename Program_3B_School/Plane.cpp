#include "Plane.h"

Plane::Plane()
{
	altitude = 0;
	takeOff = false;
	land = true;
}
int Plane::getAltitude()
{
	return altitude;
}
void Plane::setAltitude(int thisAltitude)
{
	altitude = thisAltitude;
}
bool Plane::getAirOn()
{
	return airOn;
}
void Plane::turnAirOn()
{
	airOn = true;
}
void Plane::turnAirOff()
{
	airOn = false;
}
