#pragma once
#ifndef PLANE_H
#define PLANE_H

#include "LandVehicle.h"
#include <string>
#include <vector>
#include <iostream>
class Plane : public LandVehicle
{
public:
	Plane();
	int getAltitude();
	void setAltitude(int thisAltitude);
	bool getAirOn();
	void turnAirOn();
	void turnAirOff();
private:
	int altitude;
	bool takeOff;
	bool land;
	bool airOn;
};

#endif // !PLANE_H
