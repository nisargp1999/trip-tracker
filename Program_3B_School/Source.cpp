#include "Car.h"
#include "Truck.h"
#include "Boat.h"
#include "Plane.h"
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

void printCarMenu();
void printTruckMenu();
void printBoatMenu();
void printPlaneMenu();


int main()
{
	//Output file
	ofstream fout;
	fout.open("Outputs.txt");

	//Variables...Vectors
	int userMenu, userVehicle, userInput, count, userSelection, numPass, tempStat, userCapacity, userAlt, userPropeller;
	float coFriction;
	bool endOfMenu = false;
	int userLocation;	//1.Home, 2.Airport, 3.Marina
	vector<int>	location;
	vector<int> turnsTripOne;
	vector<int> turnsTripTwo;
	vector<int> turnsTripThree;
	vector<int> vehicle;
	
	//Vehicles
	Car Lambo;				//Vehicle 1
	Car Taxi;				//...2
	Truck HaulTruck;		//...3
	Truck TankTruck;		//...4
	Boat Yacht;				//...5
	Boat Hovercraft;		//...6
	Plane AirbusA310;		//...7
	Plane Boeing747;		//...8

	//Welcome message
	cout << "Welcome, user to an amazing program where you will get to drive and fly many different types of vehicles.\nDON'T FORGET THE SEATBELT!!!" << endl;
	fout << "Welcome message" << endl;
	system("pause");
	system("CLS");


	location.push_back(1);
	fout << "User starts at home" << endl;

	//................... . . . . . . . . . .  .  .  .  .    .    .    .    .    .     .     .     .          .          .          .

	do {
		if (location.back() == 1)		//If user is at home
		{
			cout << "You are at home right now" << endl;
			cout << "1. Take the Car" << endl;
			cout << "2. Take the Truck" << endl;
			cin >> userMenu;
			

					if (userMenu == 1)							//If user selected Car
					{
						
						vehicle.push_back(1);
						fout << "User takes the car" << endl;
						do {
							system("CLS");
							cout << "You are driving Car" << endl;
							printCarMenu();
							cin >> userInput;
							//_________________________________________Menu_________________________________________
							switch (userInput) {
							case 1:			//Set age of car
								cout << "What is the age of car now?: ";
								cin >> userSelection;
								if (userSelection < Lambo.getAge())
								{
									cout << "Invalid input. Age cannot be less than last one" << endl;
									system("pause");
								}
								else if (userSelection == Lambo.getAge())
								{
									cout << "The age you entered is the same age stored earlier" << endl;
									system("pause");
								}
								else {
									Lambo.setAge(userSelection);
									cout << "Age stored successfully" << endl;
									fout << "Age stored as " << userSelection << endl;
									system("pause");
								}
								break;
							case 2:			//Set price of car
								cout << "What is the price of your car now?: ";
								cin >> userSelection;
								if (userSelection > Lambo.getPrice())
								{
									cout << "Invalid input. Price cannot be more than last one" << endl;
									system("pause");
								}
								else if (userSelection == Lambo.getPrice())
								{
									cout << "The price you enteres it the same price stored earlier" << endl;
									system("pause");
								}
								else if (userSelection < 0)
								{
									cout << "Invalid input: Price cannot be negative" << endl;
									system("Pause");
								}
								else {
									Lambo.setPrice(userSelection);
									cout << "Price stored successfully" << endl;
									fout << "Price is stored as " << userSelection << endl;
									system("pause");
								}
								break;
							case 3:			//Check balance of user
								if (vehicle.at(0) == 1)
								{
									cout << "Your balance is " << Lambo.getBalance() << endl;
								}
								else if (vehicle.at(0) == 4)
								{
									cout << "Your balance is " << TankTruck.getBalance() << endl;
								}
								system("pause");
								break;
							case 4:			//Turn engine on
								if (Lambo.getEngineOn())
								{
									cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
								}
								else {
									Lambo.turnEngineOn();
									cout << "Engine is now on" << endl;
									fout << "User turned on the engine" << endl;
								}
								system("pause");
								break;
							case 5:			//Turn engine off
								if (Lambo.getEngineOn() == false || Lambo.getSpeed() != 0 || Lambo.getAirOn() == true || Lambo.getSunroofOpen() == true || Lambo.getLightsOn())
								{
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
									}
									if (Lambo.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
									}
									if (Lambo.getAirOn() == true)
									{
										cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
									}
									if (Lambo.getSunroofOpen() == true)
									{
										cout << "Invalid input: Cannot turn engine off while sunroof is open" << endl;
									}
									if (Lambo.getLightsOn())
									{
										cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
									}
								}
								else {
									Lambo.turnEngineOff();
									cout << "Engine is now off" << endl;
									fout << "User turned off the engine" << endl;
								}
								system("pause");
								break;
							case 6:			//Turn lights on
								if (Lambo.getLightsOn() || Lambo.getEngineOn() == false)
								{
									if (Lambo.getLightsOn())
									{
										cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
									}
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
									}
								}
								else {
									Lambo.turnLightsOn();
									cout << "Lights are now on" << endl;
									fout << "User turned on the lights" << endl;
								}
								system("pause");
								break;

							case 7:			//Turn lights off
								if (Lambo.getEngineOn() == false || Lambo.getLightsOn() == false)
								{
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
									}
									if (Lambo.getLightsOn() == false)
									{
										cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
									}
								}
								else {
									Lambo.turnLightsOff();
									cout << "Lights are now off" << endl;
									fout << "User turned off the lights" << endl;
								}
								system("pause");
								break;
							case 8:			//Add Passengers
								if (Lambo.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
								}
								else {
									cout << "How many passengers do you want to add? ";
									cin >> numPass;
									if (numPass == 0)
									{
										cout << "Invalid input: Cannot add 0 passengers" << endl;
									}
									else if (Lambo.getNumPass() + numPass > 4)
									{
										cout << "Invlalid input: Cannot add passengers to have total of more than 4" << endl;
									}
									else {
										Lambo.addPass(numPass);
										cout << "Passengers added successfully" << endl;
										fout << "User added " << numPass << " passengers" << endl;
									}
								}
								system("pause");
								break;

							case 9:			//Kick Passengers
								if (Lambo.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
								}
								else {
									cout << "How many passengers do you want to kick out" << endl;
									cin >> numPass;
									if (numPass == 0)
									{
										cout << "Invalid input: Cannot kick 0 passengers" << endl;
									}
									else if (numPass > Lambo.getNumPass())
									{
										cout << "Invalid input: Cannot kick more passengers than there are" << endl;
									}
									else {
										Lambo.kickPass(numPass);
										cout << "Passengers kicked out successfully successfully" << endl;
										fout << "User kicked out " << numPass << " passengers" << endl;
									}
								}
								system("pause");
								break;
							case 10:			//Check mileage
								cout << "You car's mileage is: " << Lambo.getMileage() << " miles" << endl;
								system("pause");
								break;
							case 11:			//Set coefficient of friction
								cout << "What is the current coefficient of friction" << endl;
								cin >> coFriction;
								if (coFriction < 0)
								{
									cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
								} 
								else if (coFriction > 1)
								{
									cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
								}
								else {
									Lambo.setCoFriction(coFriction);
									cout << "Coefficient is updated successfully" << endl;
									fout << "Coefficient is updated to " << coFriction << endl;
								}
								system("pause");
								break;
							case 12:			//Check friction
								cout << "Current friction on your car is: " << Lambo.getFriction();
								system("pause");
								break;
							case 13:			//Turn air on
								if (Lambo.getAirOn() || Lambo.getEngineOn() == false || Lambo.getSunroofOpen())
								{
									if (Lambo.getAirOn())
									{
										cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
									}
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn air on if engine is off" << endl;
									}
									if (Lambo.getSunroofOpen())
									{
										cout << "Invalid input: Cannot turn air on if sunroof is open" << endl;
									}
								}
								else {
									Lambo.turnAirOn();
									cout << "Air is now on" << endl;
									fout << "User turned on the air" << endl;
								}
								system("pause");
								break;
							case 14:			//Turn air off
								if (Lambo.getAirOn() == false)
								{
									cout << "Invalid input: Cannot turn air off if it is already off" << endl;
								}
								else {
									Lambo.turnAirOff();
									cout << "Air is now off" << endl;
									fout << "User turned off the air" << endl;
								}
								system("pause");
								break;
							case 15:			//Change race car status
								cout << "Type 1 if it IS a race car and type 0 if it IS NOT a race car: ";
								cin >> tempStat;
								if (tempStat < 0 || tempStat > 1)
								{
									cout << "Invalid input: Only accepted values are 0 and 1" << endl;
								}
								else {
									Lambo.setRaceCarStatus(tempStat);
									cout << "Race Car status updated" << endl;
									if (tempStat == true)
									{
										fout << "Race car status updated to false" << endl;
									}
									else {
										fout << "Race car status updated to true" << endl;
									}
								}
								system("pause");
								break;
							case 16:			//Open sunroof
								if (Lambo.getSunroofOpen() || Lambo.getEngineOn() == false || Lambo.getAirOn())
								{
									if (Lambo.getSunroofOpen())
									{
										cout << "Invalid input: Cannot open sunroof if it is already open" << endl;
									}
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot open sunroof if engine is off" << endl;
									}
									if (Lambo.getAirOn())
									{
										cout << "Invalid input: Cannot open sunroof if A/C is on" << endl;
									}
								}
								else {
									Lambo.setSunRoofStatus(true);
									cout << "Sunroof is now open" << endl;
									fout << "User opened the sunroof" << endl;
								}
								system("pause");
								break;
							case 17:			//Close sunroof
								if (Lambo.getSunroofOpen() == false || Lambo.getEngineOn() == false)
								{
									if (Lambo.getSunroofOpen() == false)
									{
										cout << "Invalid input: Cannot close sunroof if it is already closed" << endl;
									}
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot close sunroof if engine is off" << endl;
									}
								}
								else {
									Lambo.setSunRoofStatus(false);
									cout << "Sunroof is now closed" << endl;
									fout << "User closed the sunroof" << endl;
								}
								system("pause");
								break;
							case 18:			//Turn right
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn if engine is off" << endl;
								}
								else if (Lambo.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot turn if speed is 0" << endl;
								}
								else if (Lambo.getSpeed() > 10)
								{
									cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
								}
								else {
									Lambo.turnRight(turnsTripOne);
									cout << "You have turned right" << endl;
									fout << "User turned right" << endl;
									Lambo.addMileage(10);
								}
								system("pause");
								break;
							case 19:			//Turn left
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn if engine is off" << endl;
								}
								else if (Lambo.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot turn if speed is 0" << endl;
								}
								else if (Lambo.getSpeed() > 10)
								{
									cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
								}
								else {
									Lambo.turnLeft(turnsTripOne);
									cout << "You have turned left" << endl;
									fout << "User turned left" << endl;
									Lambo.addMileage(10);
								}
								system("pause");
								break;
							case 20:			//Accelerate
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot accelerate if engine is off" << endl;
								}
								else if (Lambo.getSpeed() == 45)
								{
									cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
								}
								else {
									Lambo.accelerate();
									cout << "Speed is now increased to " << Lambo.getSpeed() << endl;
									fout << "User accelerated vehicle to " << Lambo.getSpeed() << endl;
									Lambo.addMileage(10);
								}
								system("pause");
								break;
							case 21:			//Decelerate
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot decelerate if engine is off" << endl;
								}
								else if (Lambo.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
								}
								else {
									Lambo.decelerate();
									cout << "Speed is now decreased to " << Lambo.getSpeed() << endl;
									fout << "User decelerated vehicle to " << Lambo.getSpeed() << endl;
									Lambo.addMileage(10);
								}
								system("pause");
								break;
							case 22:			//Return to last location
								for (int i = 0; i < turnsTripOne.size(); i++)
								{
									if (turnsTripOne.at(i) == 1)
									{
										turnsTripOne[i] = 3;
									}
									else {
										turnsTripOne[i] = 1;
									}
								}
								reverse(turnsTripOne.begin(), turnsTripOne.end());
								for (int i = 0; i < turnsTripOne.size(); i++)
								{
									if (turnsTripOne.at(i) == 1)
									{
										cout << "Turn left..." << endl;
										fout << "User turned left" << endl;
									}
									else {
										cout << "Turn right..." << endl;
										fout << "User turned right" << endl;
									}
								}
								location[location.size() - 1] = NULL;		//Deletes last location
								cout << "You are at home now" << endl;
								fout << "User has gone back to last location" << endl;
								system("pause");
								break;
							case 23:			//Reached destination
								if (Lambo.getEngineOn())
								{
									cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
								}
								else {
									cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
									cin >> userLocation;
									if (userLocation == 2)
									{
										location.push_back(2);
									}
									else if (userLocation == 3)
									{
										location.push_back(3);
									}
									else {
										cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
									}
									fout << "User has ended this trip" << endl;
									Lambo.addMileage(20);
								}
								system("pause");
								break;
							case 0:			//Quit
								cout << "Sorry to see you go" << endl;
								endOfMenu = true;
								system("pause");
								break;
							};
							//_________________________________________Menu_________________________________________
							if (userInput == 0)
							{
								break;
							}
						} while (userInput != 23);
					}


					else if (userMenu == 2)						//If user selected Truck
					{
						
						vehicle.push_back(4);
						fout << "User is driving a Truck now" << endl;
						do {
							system("CLS");
							cout << "You are driving Truck" << endl;
							printTruckMenu();
							cin >> userInput;

							//_________________________________________Menu_________________________________________
							switch (userInput) {
							case 1:			//Set age of Truck
								cout << "What is the age of truck now?: ";
								cin >> userSelection;
								if (userSelection < TankTruck.getAge())
								{
									cout << "Invalid input. Age cannot be less than last one" << endl;
									system("pause");
								}
								else if (userSelection == TankTruck.getAge())
								{
									cout << "The age you entered is the same age stored earlier" << endl;
									system("pause");
								}
								else if (userSelection < 0)
								{
									cout << "Invalid input: Price cannot be negative" << endl;
									system("Pause");
								}
								else {
									TankTruck.setAge(userSelection);
									cout << "Age stored successfully" << endl;
									fout << "Age is stored as " << TankTruck.getAge() << endl;
									system("pause");
								}
								break;
							case 2:			//Set price of Truck
								cout << "What is the price of your truck now?: ";
								cin >> userSelection;
								if (userSelection > TankTruck.getPrice())
								{
									cout << "Invalid input. Price cannot be more than last one" << endl;
									system("pause");
								}
								else if (userSelection == TankTruck.getPrice())
								{
									cout << "The price you enteres it the same price stored earlier" << endl;
									system("pause");
								}
								else {
									TankTruck.setPrice(userSelection);
									cout << "Price stored successfully" << endl;
									fout << "Price is stored as " << TankTruck.getPrice() << endl;
									system("pause");
								}
								break;
							case 3:			//Check balance of user
								if (vehicle.at(0) == 1)
								{
									cout << "Your balance is " << Lambo.getBalance() << endl;
								}
								else if (vehicle.at(0) == 4)
								{
									cout << "Your balance is " << TankTruck.getBalance() << endl;
								}
								system("pause");
								break;
							case 4:			//Turn engine on
								if (TankTruck.getEngineOn())
								{
									cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
								}
								else {
									TankTruck.turnEngineOn();
									cout << "Engine is now on" << endl;
									fout << "User turned on the engine" << endl;
								}
								system("pause");
								break;
							case 5:			//Turn engine off
								if (TankTruck.getEngineOn() == false || TankTruck.getSpeed() != 0 || TankTruck.getAirOn() == true || TankTruck.getLightsOn())
								{
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
									}
									if (TankTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
									}
									if (TankTruck.getAirOn() == true)
									{
										cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
									}
									if (TankTruck.getLightsOn())
									{
										cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
									}
								}
								else {
									TankTruck.turnEngineOff();
									cout << "Engine is now off" << endl;
									fout << "User turned off the engine" << endl;
								}
								system("pause");
								break;
							case 6:			//Turn lights on
								if (TankTruck.getLightsOn() || TankTruck.getEngineOn() == false)
								{
									if (TankTruck.getLightsOn())
									{
										cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
									}
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
									}
								}
								else {
									TankTruck.turnLightsOn();
									cout << "Lights are now on" << endl;
									fout << "User turned on the lights" << endl;
								}
								system("pause");
								break;

							case 7:			//Turn lights off
								if (TankTruck.getEngineOn() == false || TankTruck.getLightsOn() == false)
								{
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
									}
									if (TankTruck.getLightsOn() == false)
									{
										cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
									}
								}
								else {
									TankTruck.turnLightsOff();
									cout << "Lights are now off" << endl;
									fout << "User turned off the lights" << endl;
								}
								system("pause");
								break;
							case 8:			//Add Passengers
								if (TankTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
								}
								else {
									cout << "How many passengers do you want to add? ";
									cin >> numPass;
									if (numPass == 0)
									{
										cout << "Invalid input: Cannot add 0 passengers" << endl;
									}
									else if (TankTruck.getNumPass() + numPass > 1)
									{
										cout << "Invlalid input: Cannot add passengers to have total of more than 1" << endl;
									}
									else {
										TankTruck.addPass(numPass);
										cout << "Passengers added successfully" << endl;
										fout << "User added " << numPass << " passengers" << endl;
									}
								}
								system("pause");
								break;

							case 9:			//Kick Passengers
								if (TankTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
								}
								else {
									cout << "How many passengers do you want to kick out" << endl;
									cin >> numPass;
									if (numPass == 0)
									{
										cout << "Invalid input: Cannot kick 0 passengers" << endl;
									}
									else if (numPass > TankTruck.getNumPass())
									{
										cout << "Invalid input: Cannot kick more passengers than there are" << endl;
									}
									else {
										TankTruck.kickPass(numPass);
										cout << "Passengers kicked out successfully" << endl;
										fout << "User has kicked " << numPass << " passengers" << endl;
									}
								}
								system("pause");
								break;
							case 10:			//Check mileage
								cout << "You car's mileage is: " << TankTruck.getMileage() << " miles" << endl;
								system("pause");
								break;
							case 11:			//Set coefficient of friction
								cout << "What is the current coefficient of friction" << endl;
								cin >> coFriction;
								if (coFriction < 0)
								{
									cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
								}
								else if (coFriction > 1)
								{
									cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
								}
								else {
									TankTruck.setCoFriction(coFriction);
									cout << "Coefficient is updated successfully" << endl;
									fout << "Coefficient is updated to " << coFriction << endl;
								}
								system("pause");
								break;
							case 12:			//Check friction
								cout << "Current friction on your car is: " << TankTruck.getFriction();
								system("pause");
								break;
							case 13:			//Turn air on
								if (TankTruck.getAirOn() || TankTruck.getEngineOn() == false)
								{
									if (TankTruck.getAirOn())
									{
										cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
									}
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn air on if engine is off" << endl;
									}
								}
								else {
									TankTruck.turnAirOn();
									cout << "Air is now on" << endl;
									fout << "User turned on the air" << endl;
								}
								system("pause");
								break;
							case 14:			//Turn air off
								if (TankTruck.getAirOn() == false)
								{
									cout << "Invalid input: Cannot turn air off if it is already off" << endl;
								}
								else {
									TankTruck.turnAirOff();
									cout << "Air is now off" << endl;
									fout << "User turned off the air" << endl;
								}
								system("pause");
								break;
							case 15:			//Change diesel type status
								cout << "Type 1 if it IS a diesel type and type 0 if it IS NOT a diesel type: ";
								cin >> tempStat;
								if (tempStat < 0 || tempStat > 1)
								{
									cout << "Invalid input: Only accepted values are 0 and 1" << endl;
								}
								else {
									TankTruck.setDieselTypeStatus(tempStat);
									cout << "Diesel type status updated" << endl;
									if (tempStat)
									{
										fout << "Diesel type status is updated to true" << endl;
									}
									else
									{
										fout << "Diesel type status is updated to false" << endl;
									}
								}
								system("pause");
								break;
							case 16:			//Load cargo
								cout << "How much do you want to load?: " << endl;
								cin >> userCapacity;
								if (userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || TankTruck.getCapacity() + userCapacity > 100 || TankTruck.getSpeed() != 0)
								{
									if (100 - TankTruck.getCapacity() < userCapacity)
									{
										cout << "Invalid input: Cannot add to end up with more than 100 cargo" << endl;
									}
									if (userCapacity < 0)
									{
										cout << "Invalid input: Cannot add 0 cargo" << endl;
									}
									if (userCapacity == 0)
									{
										cout << "Invalid input: Cannot add 0 cargo" << endl;
									}
									if (TankTruck.getCapacity() > 100)
									{
										cout << "Invalid input: Cannot add more than 100 cargo" << endl;
									}
									if (TankTruck.getCapacity() + userCapacity > 100)
									{
										cout << "Invalid input: Cannot add cargo to have total of more than 100 cargo" << endl;
									}
									if (TankTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add cargo while vehicle is moving" << endl;
									}
								}
								else {
									TankTruck.loadCargo(userCapacity);
									cout << "Cargo loaded successfully" << endl;
									fout << "User added " << userCapacity << " cargo" << endl;
								}
								system("pause");
								break;
							case 17:			//Unload Cargo
								cout << "How much do you want to unload?: " << endl;
								cin >> userCapacity;
								if (userCapacity > TankTruck.getCapacity() || userCapacity < 0 || userCapacity == 0 || userCapacity > 100 ||  TankTruck.getSpeed() != 0)
								{
									if (userCapacity > TankTruck.getCapacity())
									{
										cout << "Invalid input: Cannot unload to end up with negative cargo" << endl;
									}
									if (userCapacity < 0)
									{
										cout << "Invalid input: Cannot have a negative cargo" << endl;
									}
									if (userCapacity == 0)
									{
										cout << "Invalid input: Cannot unload 0 cargo" << endl;
									}
									if (userCapacity > 100)
									{
										cout << "Invalid input: Cannot unload more than 100 cargo" << endl;
									}
									if (TankTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot unload cargo while vehicle is moving" << endl;
									}
								}
								else {
									TankTruck.loadCargo(userCapacity);
									cout << "Cargo loaded successfully" << endl;
									fout << "User took out " << userCapacity << " cargo" << endl;
								}
								system("pause");
								break;
							case 18:			//Check cargo
								cout << "Current cargo is " << TankTruck.getCapacity() << endl;
								system("pause");
								break;
							case 19:			//Turn right
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn if engine is off" << endl;
								}
								else if (TankTruck.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot turn if speed is 0" << endl;
								}
								else if (TankTruck.getSpeed() > 10)
								{
									cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
								}
								else {
									TankTruck.turnRight(turnsTripOne);
									cout << "You have turned right" << endl;
									fout << "User turned right" << endl;
									TankTruck.addMileage(10);
								}
								system("pause");
								break;
							case 20:			//Turn left
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn if engine is off" << endl;
								}
								else if (TankTruck.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot turn if speed is 0" << endl;
								}
								else if (TankTruck.getSpeed() > 10)
								{
									cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
								}
								else {
									TankTruck.turnLeft(turnsTripOne);
									cout << "You have turned left" << endl;
									fout << "User turned left" << endl;
									TankTruck.addMileage(10);
								}
								system("pause");
								break;
							case 21:			//Accelerate
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot accelerate if engine is off" << endl;
								}
								else if (TankTruck.getSpeed() == 45)
								{
									cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
								}
								else {
									TankTruck.accelerate();
									cout << "Speed is now increased to " << TankTruck.getSpeed() << endl;
									fout << "User accelerated vehicle to " << TankTruck.getSpeed() << endl;
									TankTruck.addMileage(10);
								}
								system("pause");
								break;
							case 22:			//Decelerate
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot decelerate if engine is off" << endl;
								}
								else if (TankTruck.getSpeed() == 0)
								{
									cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
								}
								else {
									TankTruck.decelerate();
									cout << "Speed is now decreased to " << TankTruck.getSpeed() << endl;
									fout << "User decelerated vehicle to " << TankTruck.getSpeed() << endl;
									TankTruck.addMileage(10);
								}
								system("pause");
								break;
							case 23:			//Return to last location
								for (int i = 0; i < turnsTripOne.size(); i++)
								{
									if (turnsTripOne.at(i) == 1)
									{
										turnsTripOne[i] = 3;
									}
									else {
										turnsTripOne[i] = 1;
									}
								}
								reverse(turnsTripOne.begin(), turnsTripOne.end());
								for (int i = 0; i < turnsTripOne.size(); i++)
								{
									if (turnsTripOne.at(i) == 1)
									{
										cout << "Turn left..." << endl;
										fout << "User turned left" << endl;
									}
									else {
										cout << "Turn right..." << endl;
										fout << "User turned right" << endl;
									}
								}
								location[location.size() - 1] = NULL;		//Deletes last location
								cout << "You are at home now" << endl;
								fout << "User has returned to last location" << endl;
								TankTruck.addMileage(20);

								system("pause");
								break;
							case 24:			//Reached destination
								if (TankTruck.getEngineOn())
								{
									cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
								}
								else {
									cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
									cin >> userLocation;
									if (userLocation == 2)
									{
										location.push_back(2);
									}
									else if (userLocation == 3)
									{
										location.push_back(3);
									}
									else {
										cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
									}
									fout << "User has reached the destination" << endl;
									TankTruck.addMileage(20);
								}
								system("Pause");
								break;
							case 0:			//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
							};
							//_________________________________________Menu_________________________________________
							if (userInput == 0)
							{
								break;
							}
						} while (userInput != 24);
					}


					else {
						cout << "Invalid input. Neither Car or Truck was selected" << endl;
					}
		}





//................... . . . . . . . . . .  .  .  .  .    .    .    .    .    .     .     .     .          .          .          .


		else if (location.back() == 2)		//If user is at Airport
		{
			cout << "You are at Airport right now" << endl;
			cout << "1. Take the Car" << endl;
			cout << "2. Take the Truck" << endl;
			cout << "3. Take the Plane" << endl;
			cin >> userMenu;

					if (userMenu == 1)						//If user selected Car
					{
						
						vehicle.push_back(2);
						fout << "User is driving a car" << endl;
						do {
							system("CLS");
							cout << "You are driving Car" << endl;
							printCarMenu();
							cin >> userInput;
							if (location.at(location.size() - 2) == 1)			//Car
							{
								//_________________________________________Menu_________________________________________
								switch (userInput) {
								case 1:			//Set age of car
									cout << "What is the age of car now?: ";
									cin >> userSelection;
									if (userSelection < Lambo.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == Lambo.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										Lambo.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 2:			//Set price of car
									cout << "What is the price of your car now?: ";
									cin >> userSelection;
									if (userSelection > Lambo.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == Lambo.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										Lambo.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:			//Check balance of user
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;
									}
									system("pause");
									break;
								case 4:			//Turn engine on
									if (Lambo.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										Lambo.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turned on the engine" << endl;
									}
									system("pause");
									break;
								case 5:			//Turn engine off
									if (Lambo.getEngineOn() == false || Lambo.getSpeed() != 0 || Lambo.getAirOn() == true || Lambo.getSunroofOpen() == true || Lambo.getLightsOn())
									{
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (Lambo.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (Lambo.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (Lambo.getSunroofOpen() == true)
										{
											cout << "Invalid input: Cannot turn engine off while sunroof is open" << endl;
										}
										if (Lambo.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										Lambo.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned off the engine" << endl;
									}
									system("pause");
									break;
								case 6:			//Turn lights on
									if (Lambo.getLightsOn() || Lambo.getEngineOn() == false)
									{
										if (Lambo.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										Lambo.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned on the ligths" << endl;
									}
									system("pause");
									break;

								case 7:			//Turn lights off
									if (Lambo.getEngineOn() == false || Lambo.getLightsOn() == false)
									{
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (Lambo.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										Lambo.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned off the lights" << endl;
									}
									system("pause");
									break;
								case 8:			//Add Passengers
									if (Lambo.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (Lambo.getNumPass() + numPass > 4)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 4" << endl;
										}
										else {
											Lambo.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User added " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;

								case 9:			//Kick Passengers
									if (Lambo.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > Lambo.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											Lambo.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 10:			//Check mileage
									cout << "You car's mileage is: " << Lambo.getMileage() << " miles" << endl;
									system("pause");
									break;
								case 11:			//Set coefficient of friction
									cout << "What is the current coefficient of friction" << endl;
									cin >> coFriction;
									if (coFriction < 0)
									{
										cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
									}
									else if (coFriction > 1)
									{
										cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
									}
									else {
										Lambo.setCoFriction(coFriction);
										cout << "Coefficient is updated successfully" << endl;
										fout << "Coefficient is updated to " << coFriction << endl;
									}
									system("pause");
									break;
								case 12:			//Check friction
									cout << "Current friction on your car is: " << Lambo.getFriction();
									system("pause");
									break;
								case 13:			//Turn air on
									if (Lambo.getAirOn() || Lambo.getEngineOn() == false || Lambo.getSunroofOpen())
									{
										if (Lambo.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
										if (Lambo.getSunroofOpen())
										{
											cout << "Invalid input: Cannot turn air on if sunroof is open" << endl;
										}
									}
									else {
										Lambo.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned on the air" << endl;
									}
									system("pause");
									break;
								case 14:			//Turn air off
									if (Lambo.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										Lambo.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned off the air" << endl;
									}
									system("pause");
									break;
								case 15:			//Change race car status
									cout << "Type 1 if it IS a race car and type 0 if it IS NOT a race car: ";
									cin >> tempStat;
									if (tempStat < 0 || tempStat > 1)
									{
										cout << "Invalid input: Only accepted values are 0 and 1" << endl;
									}
									else {
										Lambo.setRaceCarStatus(tempStat);
										cout << "Race Car status updated" << endl;
										if (tempStat)
										{
											fout << "Race Car status updated to true" << endl;
										}
										else {
											fout << "Race car status updated to false" << endl;
										}
									}
									system("pause");
									break;
								case 16:			//Open sunroof
									if (Lambo.getSunroofOpen() || Lambo.getEngineOn() == false || Lambo.getAirOn())
									{
										if (Lambo.getSunroofOpen())
										{
											cout << "Invalid input: Cannot open sunroof if it is already open" << endl;
										}
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot open sunroof if engine is off" << endl;
										}
										if (Lambo.getAirOn())
										{
											cout << "Invalid input: Cannot open sunroof if A/C is on" << endl;
										}
									}
									else {
										Lambo.setSunRoofStatus(true);
										cout << "Sunroof is now open" << endl;
										fout << "User opened sunroof" << endl;
									}
									system("pause");
									break;
								case 17:			//Close sunroof
									if (Lambo.getSunroofOpen() == false || Lambo.getEngineOn() == false)
									{
										if (Lambo.getSunroofOpen() == false)
										{
											cout << "Invalid input: Cannot close sunroof if it is already closed" << endl;
										}
										if (Lambo.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot close sunroof if engine is off" << endl;
										}
									}
									else {
										Lambo.setSunRoofStatus(false);
										cout << "Sunroof is now closed" << endl;
										fout << "User closed sunroof" << endl;
									}
									system("pause");
									break;
								case 18:			//Turn right
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Lambo.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (Lambo.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										Lambo.turnRight(turnsTripTwo);
										cout << "You have turned right" << endl;
										fout << "User turned right" << endl;
										Lambo.addMileage(10);
									}
									system("pause");
									break;
								case 19:			//Turn left
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Lambo.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (Lambo.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										Lambo.turnLeft(turnsTripTwo);
										cout << "You have turned left" << endl;
										fout << "User turned left" << endl;
										Lambo.addMileage(10);
									}
									system("pause");
									break;
								case 20:			//Accelerate
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (Lambo.getSpeed() == 45)
									{
										cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
									}
									else {
										Lambo.accelerate();
										cout << "Speed is now increased to " << Lambo.getSpeed() << endl;
										fout << "User accelerated vehicle to " << Lambo.getSpeed() << endl;
										Lambo.addMileage(10);
									}
									system("pause");
									break;
								case 21:			//Decelerate
									if (Lambo.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (Lambo.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else {
										Lambo.decelerate();
										cout << "Speed is now decreased to " << Lambo.getSpeed() << endl;
										fout << "User decelerated vehicle to " << Lambo.getSpeed() << endl;
										Lambo.addMileage(10);
									}
									system("pause");
									break;
								case 22:			//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripOne.at(i) == 1)
										{
											turnsTripOne[i] = 3;
										}
										else {
											turnsTripOne[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left" << endl;
											Lambo.addMileage(10);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right" << endl;
											Lambo.addMileage(10);
										}
									}
									if (location.back() == 1)
									{
										cout << "You are now home" << endl;
									}
									else if (location.back() == 2)
									{
										cout << "You are now at airport" << endl;
									}
									else if (location.back() == 3)
									{
										cout << "You are now at the Marina" << endl;
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									vehicle[vehicle.size() - 1] = NULL;			//Deletes last vehicle
									fout << "User returned to last location" << endl;

									system("pause");
									break;
								case 23:			//Reached destination
									if (Lambo.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
										cin >> userLocation;
										if (userLocation == 2)
										{
											location.push_back(2);
										}
										else if (userLocation == 3)
										{
											location.push_back(3);
										}
										else {
											cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
										}
										fout << "Trip is ended" << endl;
										Lambo.addMileage(20);
									}
									system("pause");
									break;
								case 0:			//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								};
								//_________________________________________Menu_________________________________________
							}
							
							else {					//Taxi
								//_________________________________________Menu_________________________________________
								switch (userInput) {
								case 1:			//Set age of car
									cout << "What is the age of car now?: ";
									cin >> userSelection;
									if (userSelection < Taxi.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == Taxi.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										Taxi.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age is stored as " << Taxi.getAge();
										system("pause");
									}
									break;
								case 2:			//Set price of car
									cout << "What is the price of your car now?: ";
									cin >> userSelection;
									if (userSelection > Taxi.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == Taxi.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										Taxi.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:			//Check balance of user
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;
									}
									system("pause");
									break;
								case 4:			//Turn engine on
									if (Taxi.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										Taxi.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turned the engine on" << endl;
									}
									system("pause");
									break;
								case 5:			//Turn engine off
									if (Taxi.getEngineOn() == false || Taxi.getSpeed() != 0 || Taxi.getAirOn() == true || Taxi.getSunroofOpen() == true || Taxi.getLightsOn())
									{
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (Taxi.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (Taxi.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (Taxi.getSunroofOpen() == true)
										{
											cout << "Invalid input: Cannot turn engine off while sunroof is open" << endl;
										}
										if (Taxi.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										Taxi.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned the engine off" << endl;
									}
									system("pause");
									break;
								case 6:			//Turn lights on
									if (Taxi.getLightsOn() || Taxi.getEngineOn() == false)
									{
										if (Taxi.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										Taxi.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned the lights on" << endl;
									}
									system("pause");
									break;

								case 7:			//Turn lights off
									if (Taxi.getEngineOn() == false || Taxi.getLightsOn() == false)
									{
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (Taxi.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										Taxi.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned off the lights" << endl;
									}
									system("pause");
									break;
								case 8:			//Add Passengers
									if (Taxi.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (Taxi.getNumPass() + numPass > 4)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 4" << endl;
										}
										else {
											Taxi.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User added " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;

								case 9:			//Kick Passengers
									if (Taxi.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > Taxi.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											Taxi.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked out " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 10:			//Check mileage
									cout << "You car's mileage is: " << Taxi.getMileage() << " miles" << endl;
									system("pause");
									break;
								case 11:			//Set coefficient of friction
									cout << "What is the current coefficient of friction" << endl;
									cin >> coFriction;
									if (coFriction < 0)
									{
										cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
									}
									else if (coFriction > 1)
									{
										cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
									}
									else {
										Taxi.setCoFriction(coFriction);
										cout << "Coefficient is updated successfully" << endl;
										fout << "Coefficient is updated to " << coFriction << endl;
									}
									system("pause");
									break;
								case 12:			//Check friction
									cout << "Current friction on your car is: " << Taxi.getFriction();
									system("pause");
									break;
								case 13:			//Turn air on
									if (Taxi.getAirOn() || Taxi.getEngineOn() == false || Taxi.getSunroofOpen())
									{
										if (Taxi.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
										if (Taxi.getSunroofOpen())
										{
											cout << "Invalid input: Cannot turn air on if sunroof is open" << endl;
										}
									}
									else {
										Taxi.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned on the air" << endl;
									}
									system("pause");
									break;
								case 14:			//Turn air off
									if (Taxi.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										Taxi.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned off the air" << endl;
									}
									system("pause");
									break;
								case 15:			//Change race car status
									cout << "Type 1 if it IS a race car and type 0 if it IS NOT a race car: ";
									cin >> tempStat;
									if (tempStat < 0 || tempStat > 1)
									{
										cout << "Invalid input: Only accepted values are 0 and 1" << endl;
									}
									else {
										Taxi.setRaceCarStatus(tempStat);
										cout << "Race Car status updated" << endl;
										if (tempStat)
										{
											fout << "Race car status updated to true" << endl;
										}
										else {
											fout << "Race car status updated to false" << endl;
										}
									}
									system("pause");
									break;
								case 16:			//Open sunroof
									if (Taxi.getSunroofOpen() || Taxi.getEngineOn() == false || Taxi.getAirOn())
									{
										if (Taxi.getSunroofOpen())
										{
											cout << "Invalid input: Cannot open sunroof if it is already open" << endl;
										}
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot open sunroof if engine is off" << endl;
										}
										if (Taxi.getAirOn())
										{
											cout << "Invalid input: Cannot open sunroof if A/C is on" << endl;
										}
									}
									else {
										Taxi.setSunRoofStatus(true);
										cout << "Sunroof is now open" << endl;
										fout << "User opened sunroof" << endl;
									}
									system("pause");
									break;
								case 17:			//Close sunroof
									if (Taxi.getSunroofOpen() == false || Taxi.getEngineOn() == false)
									{
										if (Taxi.getSunroofOpen() == false)
										{
											cout << "Invalid input: Cannot close sunroof if it is already closed" << endl;
										}
										if (Taxi.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot close sunroof if engine is off" << endl;
										}
									}
									else {
										Taxi.setSunRoofStatus(false);
										cout << "Sunroof is now closed" << endl;
										fout << "User closed sunroof" << endl;
									}
									system("pause");
									break;
								case 18:			//Turn right
									if (Taxi.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Taxi.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (Taxi.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										Taxi.turnRight(turnsTripTwo);
										cout << "You have turned right" << endl;
										fout << "User turned right" << endl;
										Taxi.addMileage(10);
									}
									system("pause");
									break;
								case 19:			//Turn left
									if (Taxi.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Taxi.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (Taxi.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										Taxi.turnLeft(turnsTripTwo);
										cout << "You have turned left" << endl;
										fout << "User turned left" << endl;
										Taxi.addMileage(10);
									}
									system("pause");
									break;
								case 20:			//Accelerate
									if (Taxi.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (Taxi.getSpeed() == 45)
									{
										cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
									}
									else {
										Taxi.accelerate();
										cout << "Speed is now increased to " << Taxi.getSpeed() << endl;
										fout << "User accelerated vehicle to " << Taxi.getSpeed() << endl;
										Taxi.addMileage(10);
									}
									system("pause");
									break;
								case 21:			//Decelerate
									if (Taxi.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (Taxi.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else {
										Taxi.decelerate();
										cout << "Speed is now decreased to " << Taxi.getSpeed() << endl;
										fout << "User decelerated vehicle to " << Taxi.getSpeed() << endl;
										Taxi.addMileage(10);
									}
									system("pause");
									break;
								case 22:			//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripOne.at(i) == 1)
										{
											turnsTripOne[i] = 3;
										}
										else {
											turnsTripOne[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left" << endl;
											Taxi.addMileage(10);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right " << endl;
											Taxi.addMileage(10);
										}
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									cout << "You are at home now" << endl;
									fout << "User has returned to last location" << endl;
									system("pause");
									break;
								case 23:			//Reached destination
									if (Taxi.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
										cin >> userLocation;
										if (userLocation == 2)
										{
											location.push_back(2);
										}
										else if (userLocation == 3)
										{
											location.push_back(3);
										}
										else {
											cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
										}
										fout << "User has ended this trip" << endl;
										Taxi.addMileage(20);
									}
									system("pause");
									break;
								case 0:			//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								};
								//_________________________________________Menu_________________________________________
							}
							if (userInput == 0)
							{
								break;
							}
						} while (userInput != 23);
					}



					else if (userMenu == 2)					//If user selected Truck
					{
						
						vehicle.push_back(3);
						do {
							system("CLS");
							cout << "You are driving Truck" << endl;
							printTruckMenu();
							cin >> userInput;

							if (location.at(location.size() - 2) == 1)				//Tank Truck
							{
								//_________________________________________Menu_________________________________________
								switch (userInput) {
								case 1:			//Set age of Truck
									cout << "What is the age of truck now?: ";
									cin >> userSelection;
									if (userSelection < TankTruck.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == TankTruck.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										TankTruck.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 2:			//Set price of Truck
									cout << "What is the price of your truck now?: ";
									cin >> userSelection;
									if (userSelection > TankTruck.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == TankTruck.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										TankTruck.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:			//Check balance of user
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;
									}
									system("pause");
									break;
								case 4:			//Turn engine on
									if (TankTruck.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										TankTruck.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turned on the engine" << endl;
									}
									system("pause");
									break;
								case 5:			//Turn engine off
									if (TankTruck.getEngineOn() == false || TankTruck.getSpeed() != 0 || TankTruck.getAirOn() == true || TankTruck.getLightsOn())
									{
										if (TankTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (TankTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (TankTruck.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (TankTruck.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										TankTruck.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned off the engine" << endl;
									}
									system("pause");
									break;
								case 6:			//Turn lights on
									if (TankTruck.getLightsOn() || TankTruck.getEngineOn() == false)
									{
										if (TankTruck.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (TankTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										TankTruck.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned on the lights" << endl;
									}
									system("pause");
									break;

								case 7:			//Turn lights off
									if (TankTruck.getEngineOn() == false || TankTruck.getLightsOn() == false)
									{
										if (TankTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (TankTruck.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										TankTruck.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned ooff the ligths" << endl;
									}
									system("pause");
									break;
								case 8:			//Add Passengers
									if (TankTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (TankTruck.getNumPass() + numPass > 1)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 1" << endl;
										}
										else {
											TankTruck.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User added " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;

								case 9:			//Kick Passengers
									if (TankTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > TankTruck.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											TankTruck.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked out " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 10:			//Check mileage
									cout << "You car's mileage is: " << TankTruck.getMileage() << " miles" << endl;
									system("pause");
									break;
								case 11:			//Set coefficient of friction
									cout << "What is the current coefficient of friction" << endl;
									cin >> coFriction;
									if (coFriction < 0)
									{
										cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
									}
									else if (coFriction > 1)
									{
										cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
									}
									else {
										TankTruck.setCoFriction(coFriction);
										cout << "Coefficient is updated successfully" << endl;
										fout << "Coefficient is updated to " << coFriction << endl;
									}
									system("pause");
									break;
								case 12:			//Check friction
									cout << "Current friction on your car is: " << TankTruck.getFriction();
									system("pause");
									break;
								case 13:			//Turn air on
									if (TankTruck.getAirOn() || TankTruck.getEngineOn() == false)
									{
										if (TankTruck.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (TankTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
									}
									else {
										TankTruck.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned on the air" << endl;
									}
									system("pause");
									break;
								case 14:			//Turn air off
									if (TankTruck.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										TankTruck.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned off the air" << endl;
									}
									system("pause");
									break;
								case 15:			//Change diesel type status
									cout << "Type 1 if it IS a diesel type and type 0 if it IS NOT a diesel type: ";
									cin >> tempStat;
									if (tempStat < 0 || tempStat > 1)
									{
										cout << "Invalid input: Only accepted values are 0 and 1" << endl;
									}
									else {
										TankTruck.setDieselTypeStatus(tempStat);
										cout << "Diesel type status updated" << endl;
										if (tempStat)
										{
											fout << "Diesel type status updated to true" << endl;
										}
										else {
											fout << "Diesel type status updated to false" << endl;
										}
									}
									system("pause");
									break;
								case 16:			//Load cargo
									cout << "How much do you want to load?: " << endl;
									cin >> userCapacity;
									if (userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || TankTruck.getCapacity() + userCapacity > 100 || TankTruck.getSpeed() != 0)
									{
										if (100 - TankTruck.getCapacity() < userCapacity)
										{
											cout << "Invalid input: Cannot add to end up with more than 100 cargo" << endl;
										}
										if (userCapacity < 0)
										{
											cout << "Invalid input: Cannot add 0 cargo" << endl;
										}
										if (userCapacity == 0)
										{
											cout << "Invalid input: Cannot add 0 cargo" << endl;
										}
										if (TankTruck.getCapacity() > 100)
										{
											cout << "Invalid input: Cannot add more than 100 cargo" << endl;
										}
										if (TankTruck.getCapacity() + userCapacity > 100)
										{
											cout << "Invalid input: Cannot add cargo to have total of more than 100 cargo" << endl;
										}
										if (TankTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot add cargo while vehicle is moving" << endl;
										}
									}
									else {
										TankTruck.loadCargo(userCapacity);
										cout << "Cargo loaded successfully" << endl;
										fout << "User added " << userCapacity << " cargo" << endl;
									}
									system("pause");
									break;
								case 17:			//Unload Cargo
									cout << "How much do you want to unload?: " << endl;
									cin >> userCapacity;
									if (userCapacity > TankTruck.getCapacity() || userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || TankTruck.getSpeed() != 0)
									{
										if (userCapacity > TankTruck.getCapacity())
										{
											cout << "Invalid input: Cannot unload to end up with negative cargo" << endl;
										}
										if (userCapacity < 0)
										{
											cout << "Invalid input: Cannot have a negative cargo" << endl;
										}
										if (userCapacity == 0)
										{
											cout << "Invalid input: Cannot unload 0 cargo" << endl;
										}
										if (userCapacity > 100)
										{
											cout << "Invalid input: Cannot unload more than 100 cargo" << endl;
										}
										if (TankTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot unload cargo while vehicle is moving" << endl;
										}
									}
									else {
										TankTruck.loadCargo(userCapacity);
										cout << "Cargo loaded successfully" << endl;
										fout << "User took out " << userCapacity << " cargo" << endl;
									}
									system("pause");
									break;
								case 18:			//Check cargo
									cout << "Current cargo is " << TankTruck.getCapacity() << endl;
									system("pause");
									break;
								case 19:			//Turn right
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (TankTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (TankTruck.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										TankTruck.turnRight(turnsTripOne);
										cout << "You have turned right" << endl;
										fout << "User turned right " << endl;
										TankTruck.addMileage(10);
									}
									system("pause");
									break;
								case 20:			//Turn left
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (TankTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (TankTruck.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										TankTruck.turnLeft(turnsTripOne);
										cout << "You have turned left" << endl;
										fout << "User turned left" << endl;
										TankTruck.addMileage(10);
									}
									system("pause");
									break;
								case 21:			//Accelerate
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (TankTruck.getSpeed() == 45)
									{
										cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
									}
									else {
										TankTruck.accelerate();
										cout << "Speed is now increased to " << TankTruck.getSpeed() << endl;
										fout << "User accelerated vehicle to " << TankTruck.getSpeed() << endl;
										TankTruck.addMileage(10);
									}
									system("pause");
									break;
								case 22:			//Decelerate
									if (TankTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (TankTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else {
										TankTruck.decelerate();
										cout << "Speed is now decreased to " << TankTruck.getSpeed() << endl;
										fout << "User decelerated vehicle to " << TankTruck.getSpeed() << endl;
									}
									system("pause");
									break;
								case 23:			//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											turnsTripTwo[i] = 3;
										}
										else {
											turnsTripTwo[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left" << endl;
											TankTruck.addMileage(10);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right" << endl;
											TankTruck.addMileage(10);
										}
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									cout << "You are at home now" << endl;
									fout << "User has returned to last location" << endl;
									TankTruck.addMileage(20);

									system("pause");
									break;
								case 24:			//Reached destination
									if (TankTruck.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
										cin >> userLocation;
										if (userLocation == 2)
										{
											location.push_back(2);
										}
										else if (userLocation == 3)
										{
											location.push_back(3);
										}
										else {
											cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
										}
										fout << "User has reached destionation" << endl;
										TankTruck.addMileage(20);
									}
									system("pause");
									break;
								case 0:			//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								};
								//_________________________________________Menu_________________________________________
							}
							else													//HaulTruck
							{
								//_________________________________________Menu_________________________________________
								switch (userInput) {
								case 1:			//Set age of Truck
									cout << "What is the age of truck now?: ";
									cin >> userSelection;
									if (userSelection < HaulTruck.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == HaulTruck.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										HaulTruck.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 2:			//Set price of Truck
									cout << "What is the price of your truck now?: ";
									cin >> userSelection;
									if (userSelection > HaulTruck.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == HaulTruck.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										HaulTruck.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:			//Check balance of user
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;
									}
									system("pause");
									break;
								case 4:			//Turn engine on
									if (HaulTruck.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										HaulTruck.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turned on the engine" << endl;
									}
									system("pause");
									break;
								case 5:			//Turn engine off
									if (HaulTruck.getEngineOn() == false || HaulTruck.getSpeed() != 0 || HaulTruck.getAirOn() == true || HaulTruck.getLightsOn())
									{
										if (HaulTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (HaulTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (HaulTruck.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (HaulTruck.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										HaulTruck.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned off the engine" << endl;
									}
									system("pause");
									break;
								case 6:			//Turn lights on
									if (HaulTruck.getLightsOn() || HaulTruck.getEngineOn() == false)
									{
										if (HaulTruck.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (HaulTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										HaulTruck.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned on the lights" << endl;
									}
									system("pause");
									break;

								case 7:			//Turn lights off
									if (HaulTruck.getEngineOn() == false || HaulTruck.getLightsOn() == false)
									{
										if (HaulTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (HaulTruck.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										HaulTruck.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned off the lights" << endl;
									}
									system("pause");
									break;
								case 8:			//Add Passengers
									if (HaulTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (HaulTruck.getNumPass() + numPass > 8)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 1" << endl;
										}
										else {
											HaulTruck.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User added " << numPass << " passengers " << endl;
										}
									}
									system("pause");
									break;

								case 9:			//Kick Passengers
									if (HaulTruck.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > HaulTruck.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											HaulTruck.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked out " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 10:			//Check mileage
									cout << "You car's mileage is: " << HaulTruck.getMileage() << " miles" << endl;
									system("pause");
									break;
								case 11:			//Set coefficient of friction
									cout << "What is the current coefficient of friction" << endl;
									cin >> coFriction;
									if (coFriction < 0)
									{
										cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
									}
									else if (coFriction > 1)
									{
										cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
									}
									else {
										HaulTruck.setCoFriction(coFriction);
										cout << "Coefficient is updated successfully" << endl;
										fout << "Coefficeient is updated to " << coFriction << endl;
									}
									system("pause");
									break;
								case 12:			//Check friction
									cout << "Current friction on your car is: " << HaulTruck.getFriction();
									system("pause");
									break;
								case 13:			//Turn air on
									if (HaulTruck.getAirOn() || HaulTruck.getEngineOn() == false)
									{
										if (HaulTruck.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (HaulTruck.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
									}
									else {
										HaulTruck.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned on the air" << endl;
									}
									system("pause");
									break;
								case 14:			//Turn air off
									if (HaulTruck.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										HaulTruck.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned off the air" << endl;
									}
									system("pause");
									break;
								case 15:			//Change diesel type status
									cout << "Type 1 if it IS a diesel type and type 0 if it IS NOT a diesel type: ";
									cin >> tempStat;
									if (tempStat < 0 || tempStat > 1)
									{
										cout << "Invalid input: Only accepted values are 0 and 1" << endl;
									}
									else {
										HaulTruck.setDieselTypeStatus(tempStat);
										cout << "Diesel type status updated" << endl;
										if (tempStat)
										{
											fout << "Diesel type status updated to true" << endl;
										}
										else {
											fout << "Diesel type status updated to false " << endl;
										}
									}
									system("pause");
									break;
								case 16:			//Load cargo
									cout << "How much do you want to load?: " << endl;
									cin >> userCapacity;
									if (userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || HaulTruck.getCapacity() + userCapacity > 100 || HaulTruck.getSpeed() != 0)
									{
										if (100 - HaulTruck.getCapacity() < userCapacity)
										{
											cout << "Invalid input: Cannot add to end up with more than 100 cargo" << endl;
										}
										if (userCapacity < 0)
										{
											cout << "Invalid input: Cannot add 0 cargo" << endl;
										}
										if (userCapacity == 0)
										{
											cout << "Invalid input: Cannot add 0 cargo" << endl;
										}
										if (HaulTruck.getCapacity() > 100)
										{
											cout << "Invalid input: Cannot add more than 100 cargo" << endl;
										}
										if (HaulTruck.getCapacity() + userCapacity > 100)
										{
											cout << "Invalid input: Cannot add cargo to have total of more than 100 cargo" << endl;
										}
										if (HaulTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot add cargo while vehicle is moving" << endl;
										}
									}
									else {
										HaulTruck.loadCargo(userCapacity);
										cout << "Cargo loaded successfully" << endl;
										fout << "User added " << userCapacity << " cargo" << endl;
									}
									system("pause");
									break;
								case 17:			//Unload Cargo
									cout << "How much do you want to unload?: " << endl;
									cin >> userCapacity;
									if (userCapacity > HaulTruck.getCapacity() || userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || HaulTruck.getSpeed() != 0)
									{
										if (userCapacity > HaulTruck.getCapacity())
										{
											cout << "Invalid input: Cannot unload to end up with negative cargo" << endl;
										}
										if (userCapacity < 0)
										{
											cout << "Invalid input: Cannot have a negative cargo" << endl;
										}
										if (userCapacity == 0)
										{
											cout << "Invalid input: Cannot unload 0 cargo" << endl;
										}
										if (userCapacity > 100)
										{
											cout << "Invalid input: Cannot unload more than 100 cargo" << endl;
										}
										if (HaulTruck.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot unload cargo while vehicle is moving" << endl;
										}
									}
									else {
										HaulTruck.loadCargo(userCapacity);
										cout << "Cargo unloaded successfully" << endl;
										fout << "User took out " << userCapacity << " cargo" << endl;
									}
									system("pause");
									break;
								case 18:			//Check cargo
									cout << "Current cargo is " << HaulTruck.getCapacity() << endl;
									system("pause");
									break;
								case 19:			//Turn right
									if (HaulTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (HaulTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (HaulTruck.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										HaulTruck.turnRight(turnsTripOne);
										cout << "You have turned right" << endl;
										fout << "User turned right " << endl;
										HaulTruck.addMileage(10);
									}
									system("pause");
									break;
								case 20:			//Turn left
									if (HaulTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (HaulTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else if (HaulTruck.getSpeed() > 10)
									{
										cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
									}
									else {
										HaulTruck.turnLeft(turnsTripOne);
										cout << "You have turned left" << endl;
										fout << "User turned left " << endl;
										HaulTruck.addMileage(10);
									}
									system("pause");
									break;
								case 21:			//Accelerate
									if (HaulTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (HaulTruck.getSpeed() == 45)
									{
										cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
									}
									else {
										HaulTruck.accelerate();
										cout << "Speed is now increased to " << HaulTruck.getSpeed() << endl;
										fout << "User increased speed to " << HaulTruck.getSpeed() << endl;
										HaulTruck.addMileage(10);
									}
									system("pause");
									break;
								case 22:			//Decelerate
									if (HaulTruck.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (HaulTruck.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else {
										HaulTruck.decelerate();
										cout << "Speed is now decreased to " << HaulTruck.getSpeed() << endl;
										fout << "Speed is decreased to " << HaulTruck.getSpeed() << endl;
										HaulTruck.addMileage(10);
									}
									system("pause");
									break;
								case 23:			//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											turnsTripTwo[i] = 3;
										}
										else {
											turnsTripTwo[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left " << endl;
											HaulTruck.addMileage(10);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right " << endl;
											HaulTruck.addMileage(10);
										}
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									cout << "You are at home now" << endl;
									fout << "User has returned to last lcoation" << endl;
									HaulTruck.addMileage(20);
									system("pause");
									break;
								case 24:			//Reached destination
									if (HaulTruck.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
										cin >> userLocation;
										if (userLocation == 2)
										{
											location.push_back(2);
										}
										else if (userLocation == 3)
										{
											location.push_back(3);
										}
										else {
											cout << "Invalid input: Only values acceptable are 2 or 3" << endl;

										}
										fout << "User has ended this trip" << endl;
										HaulTruck.addMileage(20);
									}
									system("pause");
									break;
								case 0:			//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								};
								//_________________________________________Menu_________________________________________
							}
							if (userInput == 0)
							{
								break;
							}
						} while (userInput != 24);
					}



					else if (userMenu == 3)					//If user selected Plane
					{
						
						vehicle.push_back(7);
						do {

							system("CLS");
							cout << "You are driving Plane" << endl;
							printPlaneMenu();
							cin >> userInput;

							if (location.at(location.size() - 2) == 1)				//Airbus
							{
								//_________________________________________Menu_________________________________________
								switch (userInput)
								{
								case 1:						//Set Age
									cout << "What is the age of plane now?: ";
									cin >> userSelection;
									if (userSelection < AirbusA310.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == AirbusA310.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										AirbusA310.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 2:						//Set price
									cout << "What is the price of your plane now?: ";
									cin >> userSelection;
									if (userSelection > AirbusA310.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == AirbusA310.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										AirbusA310.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:						//Check balance
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;
									}
									system("pause");
									break;
								case 4:						//Turn engine on
									if (AirbusA310.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										AirbusA310.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turne don the engine" << endl;
									}
									system("pause");
									break;
								case 5:						//Turn engine off
									if (AirbusA310.getEngineOn() == false || AirbusA310.getSpeed() != 0 || AirbusA310.getAirOn() == true || AirbusA310.getLightsOn())
									{
										if (AirbusA310.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (AirbusA310.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (AirbusA310.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (AirbusA310.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										AirbusA310.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned off the engine" << endl;
									}
									system("pause");
									break;
								case 6:						//Turn lights on
									if (AirbusA310.getLightsOn() || AirbusA310.getEngineOn() == false)
									{
										if (AirbusA310.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (AirbusA310.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										AirbusA310.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned on the lights" << endl;
									}
									system("pause");
									break;
								case 7:						//Turn lights off
									if (AirbusA310.getEngineOn() == false || AirbusA310.getLightsOn() == false)
									{
										if (AirbusA310.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (AirbusA310.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										AirbusA310.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned off the lights" << endl;
									}
									system("pause");
									break;
								case 8:						//Add passengers
									if (AirbusA310.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (AirbusA310.getNumPass() + numPass > 200)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 200" << endl;
										}
										else {
											AirbusA310.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User adds " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 9:						//Kick passengers
									if (AirbusA310.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > AirbusA310.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											AirbusA310.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked out " << numPass << " passengers" << endl;
										}
									}
									system("pause");
									break;
								case 10:					//Turn air on
									if (AirbusA310.getAirOn() || AirbusA310.getEngineOn() == false)
									{
										if (AirbusA310.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (AirbusA310.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
									}
									else {
										AirbusA310.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned the air on" << endl;
									}
									system("pause");
									break;
								case 11:					//Turn air off
									if (AirbusA310.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										AirbusA310.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned the air off" << endl;
									}
									system("pause");
									break;
								case 12:					//Check altitude
									cout << "Your plane's altitude is " << AirbusA310.getAltitude() << " ft" << endl;
									system("Pause");
									break;
								case 13:					//Change altitude
									if (AirbusA310.getAltitude() >= 15000 && AirbusA310.getAltitude() <= 35000)						//If in air
									{
										if (AirbusA310.getSpeed() > 140)
										{
											cout << "Invalid input: Cannot change altitude if speed is 141 - 500 mph" << endl;
										}
										else {
											cout << "What would you like to set altitude to: ";
											cin >> userAlt;
											if (userAlt < 15000)
											{
												cout << "Invalid input: Cannot set the altitude less than 15000 without selecting to land plane" << endl;
											}
											else if (userAlt > 35000)
											{
												cout << "Invalid input: Cannot set the altitude more than 35000" << endl;
											}
											else {
												AirbusA310.setAltitude(userAlt);
												cout << "Altitude is set successfully" << endl;
												fout << "User changed altitude to " << userAlt << endl;
											}
										}
									}
									else {				//If on land
										cout << "Invalid input. Cannot change altitude if plane is on land. First take off" << endl;
									}
									system("pause");
									break;
								case 14:					//Take off
									if (AirbusA310.getEngineOn() == false)
									{
										cout << "Invalid input. Cannot fly plane if engine is off" << endl;
									}
									else {
										if (AirbusA310.getAltitude() > 15000)
										{
											cout << "Invalid input. Cannot take off if already in air" << endl;
										}
										else if (AirbusA310.getSpeed() < 100)
										{
											cout << "Invalid input. Cannot take off if speed is less than 100 mph" << endl;
										}
										else if (AirbusA310.getSpeed() > 140)
										{
											cout << "Invalid input. Cannot take off if speed is more than 140 mph" << endl;
										}
										else {
											cout << "Set altitude between 15,000 and 35,000" << endl;
											cin >> userAlt;
											if (userAlt < 15000)
											{
												cout << "Invalid input. Cannot change altitude to be less than 15,000" << endl;
											}
											else if (userAlt > 35000)
											{
												cout << "Invalid input. Cannot change altitude to be mroe than 35,000" << endl;
											}
											else {
												AirbusA310.setAltitude(userAlt);
												cout << "You are now in air" << endl;
												fout << "Plane has taken off" << endl;
												AirbusA310.addMileage(100);

											}
										}
									}
									system("pause");
									break;
								case 15:					//Land
									if (AirbusA310.getAltitude() != 0) {
										if (AirbusA310.getSpeed() > 140)
										{
											cout << "Invalid input: Cannot land if speed is more than 140 mph" << endl;
										}
										else {
											AirbusA310.setAltitude(0);
											cout << "Landed successfully" << endl;
											fout << "Plane has landed" << endl;
											AirbusA310.addMileage(100);
										}
									}
									else {
										cout << "Invalid input: Cannot land if plane if it is already on ground" << endl;
									}
									system("Pause");
									break;
								case 16:					//Turn right
									if (AirbusA310.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (AirbusA310.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else {
										AirbusA310.turnRight(turnsTripTwo);
										cout << "You have turned right" << endl;
										fout << "User turned right" << endl;
										AirbusA310.addMileage(100);
									}
									system("pause");
									break;
								case 17:					//Turn left
									if (AirbusA310.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (AirbusA310.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else {
										AirbusA310.turnLeft(turnsTripTwo);
										cout << "You have turned left" << endl;
										fout << "User turned left " << endl;
									}
									system("pause");
									break;
								case 18:					//Accelerate
									if (AirbusA310.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (AirbusA310.getAltitude() == 0) {
										if (AirbusA310.getSpeed() == 140)
										{
											cout << "Invalid input: Cannot accelerate to more than 140 mph" << endl;
										}
										else {
											AirbusA310.accelerate();
											cout << "Speed is now increased to " << AirbusA310.getSpeed() << endl;
											fout << "User has increased speed to " << AirbusA310.getSpeed() << endl;
											AirbusA310.addMileage(100);
										}
									}
									else if (AirbusA310.getSpeed() == 500)
									{
										cout << "Invalid input: Cannot accelerate to more than 500 mph while in air" << endl;
									}
									else {
										AirbusA310.accelerate();
										cout << "Speed is now increased to " << AirbusA310.getSpeed() << endl;
										fout << "User has increased speed to " << AirbusA310.getSpeed() << endl;
										AirbusA310.addMileage(100);
									}
									system("pause");
									break;
								case 19:					//Decelerate
									if (AirbusA310.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (AirbusA310.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else if (AirbusA310.getAltitude() >= 15000 && AirbusA310.getAltitude() <= 35000) {
										if (AirbusA310.getSpeed() == 120)
										{
											cout << "Invalid input: Cannot decelerate to less than 120 mph while in air" << endl;
										}
										else
										{
											AirbusA310.decelerate();
											cout << "Speed is now decreased to " << AirbusA310.getSpeed() << endl;
											fout << "User has decreased speed to " << AirbusA310.getSpeed() << endl;
											AirbusA310.addMileage(100);
										}
									}
									else {
										AirbusA310.decelerate();
										cout << "Speed is now decreased to " << AirbusA310.getSpeed() << endl;
										fout << "User has decreased speed to " << AirbusA310.getSpeed() << endl;
										AirbusA310.addMileage(100);
									}
									system("pause");
									break;
								case 20:					//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											turnsTripTwo[i] = 3;
										}
										else {
											turnsTripTwo[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left" << endl;
											AirbusA310.addMileage(100);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right " << endl;
											AirbusA310.addMileage(100);
										}
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									cout << "You are at home now" << endl;
									fout << "User has returned to last location" << endl;
									AirbusA310.addMileage(100);
									system("pause");
									break;
								case 21:					//Reached destination
									if (AirbusA310.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										location.push_back(2);
										fout << "User has ended this trip" << endl;
									}

									system("pause");
									break;
								case 22:					//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								}
								//_________________________________________Menu_________________________________________
							}
							else									//Boeing
							{
								//_________________________________________Menu_________________________________________
								switch (userInput)
								{
								case 1:						//Set Age
									cout << "What is the age of plane now?: ";
									cin >> userSelection;
									if (userSelection < Boeing747.getAge())
									{
										cout << "Invalid input. Age cannot be less than last one" << endl;
										system("pause");
									}
									else if (userSelection == Boeing747.getAge())
									{
										cout << "The age you entered is the same age stored earlier" << endl;
										system("pause");
									}
									else {
										Boeing747.setAge(userSelection);
										cout << "Age stored successfully" << endl;
										fout << "Age is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 2:						//Set price
									cout << "What is the price of your plane now?: ";
									cin >> userSelection;
									if (userSelection > Boeing747.getPrice())
									{
										cout << "Invalid input. Price cannot be more than last one" << endl;
										system("pause");
									}
									else if (userSelection == Boeing747.getPrice())
									{
										cout << "The price you enteres it the same price stored earlier" << endl;
										system("pause");
									}
									else if (userSelection < 0)
									{
										cout << "Invalid input: Price cannot be negative" << endl;
										system("Pause");
									}
									else {
										Boeing747.setPrice(userSelection);
										cout << "Price stored successfully" << endl;
										fout << "Price is stored as " << userSelection << endl;
										system("pause");
									}
									break;
								case 3:						//Check balance
									if (vehicle.at(0) == 1)
									{
										cout << "Your balance is " << Lambo.getBalance() << endl;		//Don't change
									}
									else if (vehicle.at(0) == 4)
									{
										cout << "Your balance is " << TankTruck.getBalance() << endl;	//Don't change
									}
									system("pause");
									break;
								case 4:						//Turn engine on
									if (Boeing747.getEngineOn())
									{
										cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
									}
									else {
										Boeing747.turnEngineOn();
										cout << "Engine is now on" << endl;
										fout << "User turned on the engine " << endl;
									}
									system("pause");
									break;
								case 5:						//Turn engine off
									if (Boeing747.getEngineOn() == false || Boeing747.getSpeed() != 0 || Boeing747.getAirOn() == true || Boeing747.getLightsOn())
									{
										if (Boeing747.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
										}
										if (Boeing747.getSpeed() != 0)
										{
											cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
										}
										if (Boeing747.getAirOn() == true)
										{
											cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
										}
										if (Boeing747.getLightsOn())
										{
											cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
										}
									}
									else {
										Boeing747.turnEngineOff();
										cout << "Engine is now off" << endl;
										fout << "User turned off the engine" << endl;
									}
									system("pause");
									break;
								case 6:						//Turn lights on
									if (Boeing747.getLightsOn() || Boeing747.getEngineOn() == false)
									{
										if (Boeing747.getLightsOn())
										{
											cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
										}
										if (Boeing747.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
										}
									}
									else {
										Boeing747.turnLightsOn();
										cout << "Lights are now on" << endl;
										fout << "User turned on the lights " << endl;
									}
									system("pause");
									break;
								case 7:						//Turn lights off
									if (Boeing747.getEngineOn() == false || Boeing747.getLightsOn() == false)
									{
										if (Boeing747.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
										}
										if (Boeing747.getLightsOn() == false)
										{
											cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
										}
									}
									else {
										Boeing747.turnLightsOff();
										cout << "Lights are now off" << endl;
										fout << "User turned off the lights " << endl;
									}
									system("pause");
									break;
								case 8:						//Add passengers
									if (Boeing747.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
									}
									else {
										cout << "How many passengers do you want to add? ";
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot add 0 passengers" << endl;
										}
										else if (Boeing747.getNumPass() + numPass > 200)
										{
											cout << "Invlalid input: Cannot add passengers to have total of more than 200" << endl;
										}
										else {
											Boeing747.addPass(numPass);
											cout << "Passengers added successfully" << endl;
											fout << "User added " << numPass << " passengers " << endl;
										}
									}
									system("pause");
									break;
								case 9:						//Kick passengers
									if (Boeing747.getSpeed() != 0)
									{
										cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
									}
									else {
										cout << "How many passengers do you want to kick out" << endl;
										cin >> numPass;
										if (numPass == 0)
										{
											cout << "Invalid input: Cannot kick 0 passengers" << endl;
										}
										else if (numPass > Boeing747.getNumPass())
										{
											cout << "Invalid input: Cannot kick more passengers than there are" << endl;
										}
										else {
											Boeing747.kickPass(numPass);
											cout << "Passengers kicked out successfully" << endl;
											fout << "User kicked out " << numPass << " passengers " << endl;
										}
									}
									system("pause");
									break;
								case 10:					//Turn air on
									if (Boeing747.getAirOn() || Boeing747.getEngineOn() == false)
									{
										if (Boeing747.getAirOn())
										{
											cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
										}
										if (Boeing747.getEngineOn() == false)
										{
											cout << "Invalid input: Cannot turn air on if engine is off" << endl;
										}
									}
									else {
										Boeing747.turnAirOn();
										cout << "Air is now on" << endl;
										fout << "User turned on the air " << endl;
									}
									system("pause");
									break;
								case 11:					//Turn air off
									if (Boeing747.getAirOn() == false)
									{
										cout << "Invalid input: Cannot turn air off if it is already off" << endl;
									}
									else {
										Boeing747.turnAirOff();
										cout << "Air is now off" << endl;
										fout << "User turned off the air" << endl;
									}
									system("pause");
									break;
								case 12:					//Check altitude
									cout << "Your plane's altitude is " << Boeing747.getAltitude() << " ft" << endl;
									system("Pause");
									break;
								case 13:					//Change altitude
									if (Boeing747.getAltitude() >= 15000 && Boeing747.getAltitude() <= 35000)						//If in air
									{
										if (Boeing747.getSpeed() > 140)
										{
											cout << "Invalid input: Cannot change altitude if speed is 141 - 500 mph" << endl;
										}
										else {
											cout << "What would you like to set altitude to: ";
											cin >> userAlt;
											if (userAlt < 15000)
											{
												cout << "Invalid input: Cannot set the altitude less than 15000 without selecting to land plane" << endl;
											}
											else if (userAlt > 35000)
											{
												cout << "Invalid input: Cannot set the altitude more than 35000" << endl;
											}
											else {
												Boeing747.setAltitude(userAlt);
												cout << "Altitude is set successfully" << endl;
												fout << "User changed altitude to " << userAlt << endl;
												Boeing747.addMileage(200);
											}
										}
									}
									else {				//If on land
										cout << "Invalid input. Cannot change altitude if plane is on land. First take off" << endl;
									}
									system("pause");
									break;
								case 14:					//Take off
									if (Boeing747.getEngineOn() == false)
									{
										cout << "Invalid input. Cannot fly plane if engine is off" << endl;
									}
									else {
										if (Boeing747.getAltitude() > 15000)
										{
											cout << "Invalid input. Cannot take off if already in air" << endl;
										}
										else if (Boeing747.getSpeed() < 100)
										{
											cout << "Invalid input. Cannot take off if speed is less than 100 mph" << endl;
										}
										else if (Boeing747.getSpeed() > 140)
										{
											cout << "Invalid input. Cannot take off if speed is more than 140 mph" << endl;
										}
										else {
											cout << "Set altitude between 15,000 and 35,000" << endl;
											cin >> userAlt;
											if (userAlt < 15000)
											{
												cout << "Invalid input. Cannot change altitude to be less than 15,000" << endl;
											}
											else if (userAlt > 35000)
											{
												cout << "Invalid input. Cannot change altitude to be mroe than 35,000" << endl;
											}
											else {
												Boeing747.setAltitude(userAlt);
												cout << "You are now in air" << endl;
												fout << "Plane has taken off " << endl;
												Boeing747.addMileage(200);

											}
										}
									}
									system("pause");
									break;
								case 15:					//Land
									if (Boeing747.getAltitude() != 0)
									{
										if (Boeing747.getSpeed() > 140)
										{
											cout << "Invalid input: Cannot land if speed is more than 140 mph" << endl;
										}
										else {
											Boeing747.setAltitude(0);
											cout << "Landed successfully" << endl;
											fout << "Plane has landed" << endl;
											Boeing747.addMileage(200);
										}
									}
									else {
										cout << "Invalid input: Cannot land if plane is already on ground" << endl;
									}
									system("Pause");
									break;
								case 16:					//Turn right
									if (Boeing747.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Boeing747.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else {
										Boeing747.turnRight(turnsTripTwo);
										cout << "You have turned right" << endl;
										fout << "User turned right" << endl;
										Boeing747.addMileage(200);
									}
									system("pause");
									break;
								case 17:					//Turn left
									if (Boeing747.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot turn if engine is off" << endl;
									}
									else if (Boeing747.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot turn if speed is 0" << endl;
									}
									else {
										Boeing747.turnLeft(turnsTripTwo);
										cout << "You have turned left" << endl;
										fout << "User turned left " << endl;
										Boeing747.addMileage(200);
									}
									system("pause");
									break;
								case 18:					//Accelerate
									if (Boeing747.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot accelerate if engine is off" << endl;
									}
									else if (Boeing747.getAltitude() == 0) {
										if (Boeing747.getSpeed() == 140)
										{
											cout << "Invalid input: Cannot accelerate to more than 140 mph" << endl;
										}
										else {
											Boeing747.accelerate();
											cout << "Speed is now increased to " << Boeing747.getSpeed() << endl;
											fout << "User increased speed to " << Boeing747.getSpeed() << endl;
											Boeing747.addMileage(200);
										}
									}
									else if (Boeing747.getSpeed() == 500)
									{
										cout << "Invalid input: Cannot accelerate to more than 500 mph while in air" << endl;
									}
									else {
										Boeing747.accelerate();
										cout << "Speed is now increased to " << Boeing747.getSpeed() << endl;
										fout << "User increased speed to " << Boeing747.getSpeed() << endl;
										Boeing747.addMileage(200);
									}
									system("pause");
									break;
								case 19:					//Decelerate
									if (Boeing747.getEngineOn() == false)
									{
										cout << "Invalid input: Cannot decelerate if engine is off" << endl;
									}
									else if (Boeing747.getSpeed() == 0)
									{
										cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
									}
									else if (Boeing747.getAltitude() >= 15000 && Boeing747.getAltitude() <= 35000) {
										if (Boeing747.getSpeed() == 120)
										{
											cout << "Invalid input: Cannot decelerate to less than 120 mph while in air" << endl;
										}

										else
										{
											Boeing747.decelerate();
											cout << "Speed is now decreased to " << Boeing747.getSpeed() << endl;
											fout << "User decreased speed to " << Boeing747.getSpeed() << endl;
											Boeing747.addMileage(200);
										}
									}
									else {
										Boeing747.decelerate();
										cout << "Speed is now decreased to " << Boeing747.getSpeed() << endl;
										fout << "User decreased speed to " << Boeing747.getSpeed() << endl;
										Boeing747.addMileage(200);
									}
									system("pause");
									break;
								case 20:					//Return to last location
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											turnsTripTwo[i] = 3;
										}
										else {
											turnsTripTwo[i] = 1;
										}
									}
									reverse(turnsTripTwo.begin(), turnsTripTwo.end());
									for (int i = 0; i < turnsTripTwo.size(); i++)
									{
										if (turnsTripTwo.at(i) == 1)
										{
											cout << "Turn left..." << endl;
											fout << "User turned left " << endl;
											Boeing747.addMileage(200);
										}
										else {
											cout << "Turn right..." << endl;
											fout << "User turned right " << endl;
											Boeing747.addMileage(200);
										}
									}
									location[location.size() - 1] = NULL;		//Deletes last location
									cout << "You are at home now" << endl;
									fout << "User has returned to last location" << endl;
									Boeing747.addMileage(200);
									system("pause");
									break;
								case 21:					//Reached destination
									if (Boeing747.getEngineOn())
									{
										cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
									}
									else {
										location.push_back(2);
										fout << "User has ended this trip" << endl;
										Boeing747.addMileage(200);
									}
									system("pause");
									break;
								case 22:					//Quit
									cout << "Sorry to see you go" << endl;
									endOfMenu = true;
									system("pause");
									break;
								}
								//_________________________________________Menu_________________________________________
							}
						} while (userInput != 21);
					}



					else {
						cout << "Invalid input. Neight Car, or truck, or plane was selected" << endl;
					}




			system("pause");
		}




//................... . . . . . . . . . .  .  .  .  .    .    .    .    .    .     .     .     .          .          .          .



		else if (location.back() == 3)		//If user is at Marina
		{
			cout << "You are at Marina right now" << endl;
			cout << "1. Take the Car" << endl;
			cout << "2. Take the Truck" << endl;
			cout << "3. Take the Boat" << endl;
			cin >> userMenu;

			if (userMenu == 1)						//If user selected Car
			{
				
				vehicle.push_back(2);
				do {
					system("CLS");
					cout << "You are driving Car" << endl;
					printCarMenu();
					cin >> userInput;
					if (location.at(location.size() - 2) == 1)			//Car
					{
						//_________________________________________Menu_________________________________________
						switch (userInput) {
						case 1:			//Set age of car
							cout << "What is the age of car now?: ";
							cin >> userSelection;
							if (userSelection < Lambo.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == Lambo.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								Lambo.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 2:			//Set price of car
							cout << "What is the price of your car now?: ";
							cin >> userSelection;
							if (userSelection > Lambo.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == Lambo.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								Lambo.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:			//Check balance of user
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;
							}
							system("pause");
							break;
						case 4:			//Turn engine on
							if (Lambo.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								Lambo.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turned on the engine" << endl;
							}
							system("pause");
							break;
						case 5:			//Turn engine off
							if (Lambo.getEngineOn() == false || Lambo.getSpeed() != 0 || Lambo.getAirOn() == true || Lambo.getSunroofOpen() == true || Lambo.getLightsOn())
							{
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (Lambo.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (Lambo.getAirOn() == true)
								{
									cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
								}
								if (Lambo.getSunroofOpen() == true)
								{
									cout << "Invalid input: Cannot turn engine off while sunroof is open" << endl;
								}
								if (Lambo.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								Lambo.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned off the engine" << endl;
							}
							system("pause");
							break;
						case 6:			//Turn lights on
							if (Lambo.getLightsOn() || Lambo.getEngineOn() == false)
							{
								if (Lambo.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								Lambo.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned on the lights" << endl;
							}
							system("pause");
							break;

						case 7:			//Turn lights off
							if (Lambo.getEngineOn() == false || Lambo.getLightsOn() == false)
							{
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (Lambo.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								Lambo.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned off the lights" << endl;
							}
							system("pause");
							break;
						case 8:			//Add Passengers
							if (Lambo.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (Lambo.getNumPass() + numPass > 4)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 4" << endl;
								}
								else {
									Lambo.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User added " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;

						case 9:			//Kick Passengers
							if (Lambo.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > Lambo.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									Lambo.kickPass(numPass);
									cout << "Passengers kicked out successfully successfully" << endl;
									fout << "User kicked out " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:			//Check mileage
							cout << "You car's mileage is: " << Lambo.getMileage() << " miles" << endl;
							system("pause");
							break;
						case 11:			//Set coefficient of friction
							cout << "What is the current coefficient of friction" << endl;
							cin >> coFriction;
							if (coFriction < 0)
							{
								cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
							}
							else if (coFriction > 1)
							{
								cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
							}
							else {
								Lambo.setCoFriction(coFriction);
								cout << "Coefficient is updated successfully" << endl;
								fout << "Coefficient is updated to " << coFriction << endl;
							}
							system("pause");
							break;
						case 12:			//Check friction
							cout << "Current friction on your car is: " << Lambo.getFriction();
							system("pause");
							break;
						case 13:			//Turn air on
							if (Lambo.getAirOn() || Lambo.getEngineOn() == false || Lambo.getSunroofOpen())
							{
								if (Lambo.getAirOn())
								{
									cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
								}
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn air on if engine is off" << endl;
								}
								if (Lambo.getSunroofOpen())
								{
									cout << "Invalid input: Cannot turn air on if sunroof is open" << endl;
								}
							}
							else {
								Lambo.turnAirOn();
								cout << "Air is now on" << endl;
								fout << "User turned on the air" << endl;
							}
							system("pause");
							break;
						case 14:			//Turn air off
							if (Lambo.getAirOn() == false)
							{
								cout << "Invalid input: Cannot turn air off if it is already off" << endl;
							}
							else {
								Lambo.turnAirOff();
								cout << "Air is now off" << endl;
								fout << "User turned off the air" << endl;
							}
							system("pause");
							break;
						case 15:			//Change race car status
							cout << "Type 1 if it IS a race car and type 0 if it IS NOT a race car: ";
							cin >> tempStat;
							if (tempStat < 0 || tempStat > 1)
							{
								cout << "Invalid input: Only accepted values are 0 and 1" << endl;
							}
							else {
								Lambo.setRaceCarStatus(tempStat);
								cout << "Race Car status updated" << endl;
								if (tempStat == true)
								{
									fout << "Race car status updated to false" << endl;
								}
								else {
									fout << "Race car status updated to true" << endl;
								}
							}
							system("pause");
							break;
						case 16:			//Open sunroof
							if (Lambo.getSunroofOpen() || Lambo.getEngineOn() == false || Lambo.getAirOn())
							{
								if (Lambo.getSunroofOpen())
								{
									cout << "Invalid input: Cannot open sunroof if it is already open" << endl;
								}
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot open sunroof if engine is off" << endl;
								}
								if (Lambo.getAirOn())
								{
									cout << "Invalid input: Cannot open sunroof if A/C is on" << endl;
								}
							}
							else {
								Lambo.setSunRoofStatus(true);
								cout << "Sunroof is now open" << endl;
								fout << "User opened the sunroof" << endl;
							}
							system("pause");
							break;
						case 17:			//Close sunroof
							if (Lambo.getSunroofOpen() == false || Lambo.getEngineOn() == false)
							{
								if (Lambo.getSunroofOpen() == false)
								{
									cout << "Invalid input: Cannot close sunroof if it is already closed" << endl;
								}
								if (Lambo.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot close sunroof if engine is off" << endl;
								}
							}
							else {
								Lambo.setSunRoofStatus(false);
								cout << "Sunroof is now closed" << endl;
								fout << "User closed the sunroof" << endl;
							}
							system("pause");
							break;
						case 18:			//Turn right
							if (Lambo.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Lambo.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (Lambo.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								Lambo.turnRight(turnsTripOne);
								cout << "You have turned right" << endl;
								fout << "User turned right" << endl;
								Lambo.addMileage(10);
							}
							system("pause");
							break;
						case 19:			//Turn left
							if (Lambo.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Lambo.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (Lambo.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								Lambo.turnLeft(turnsTripOne);
								cout << "You have turned left" << endl;
								fout << "User turned left" << endl;
								Lambo.addMileage(10);
							}
							system("pause");
							break;
						case 20:			//Accelerate
							if (Lambo.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (Lambo.getSpeed() == 45)
							{
								cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
							}
							else {
								Lambo.accelerate();
								cout << "Speed is now increased to " << Lambo.getSpeed() << endl;
								fout << "User accelerated vehicle to " << Lambo.getSpeed() << endl;
								Lambo.addMileage(10);
							}
							system("pause");
							break;
						case 21:			//Decelerate
							if (Lambo.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (Lambo.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
							}
							else {
								Lambo.decelerate();
								cout << "Speed is now decreased to " << Lambo.getSpeed() << endl;
								fout << "User decelerated vehicle to " << Lambo.getSpeed() << endl;
								Lambo.addMileage(10);
							}
							system("pause");
							break;
						case 22:			//Return to last location
							for (int i = 0; i < turnsTripOne.size(); i++)
							{
								if (turnsTripOne.at(i) == 1)
								{
									turnsTripOne[i] = 3;
								}
								else {
									turnsTripOne[i] = 1;
								}
							}
							reverse(turnsTripOne.begin(), turnsTripOne.end());
							for (int i = 0; i < turnsTripOne.size(); i++)
							{
								if (turnsTripOne.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left" << endl;
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right" << endl;
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							cout << "You are at home now" << endl;
							fout << "User has gone back to last location" << endl;
							system("pause");
							break;
						case 23:			//Reached destination
							if (Lambo.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
								cin >> userLocation;
								if (userLocation == 2)
								{
									location.push_back(2);
								}
								else if (userLocation == 3)
								{
									location.push_back(3);
								}
								else {
									cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
								}
								fout << "User has ended this trip" << endl;
								Lambo.addMileage(20);
							}
							system("pause");
							break;
						case 0:			//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						};
						//_________________________________________Menu_________________________________________
					}

					else {												//Taxi
						//_________________________________________Menu_________________________________________
						switch (userInput) {
						case 1:			//Set age of car
							cout << "What is the age of car now?: ";
							cin >> userSelection;
							if (userSelection < Taxi.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == Taxi.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								Taxi.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age is stored as " << Taxi.getAge();
								system("pause");
							}
							break;
						case 2:			//Set price of car
							cout << "What is the price of your car now?: ";
							cin >> userSelection;
							if (userSelection > Taxi.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == Taxi.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								Taxi.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:			//Check balance of user
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;
							}
							system("pause");
							break;
						case 4:			//Turn engine on
							if (Taxi.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								Taxi.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turned the engine on" << endl;
							}
							system("pause");
							break;
						case 5:			//Turn engine off
							if (Taxi.getEngineOn() == false || Taxi.getSpeed() != 0 || Taxi.getAirOn() == true || Taxi.getSunroofOpen() == true || Taxi.getLightsOn())
							{
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (Taxi.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (Taxi.getAirOn() == true)
								{
									cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
								}
								if (Taxi.getSunroofOpen() == true)
								{
									cout << "Invalid input: Cannot turn engine off while sunroof is open" << endl;
								}
								if (Taxi.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								Taxi.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned the engine off" << endl;
							}
							system("pause");
							break;
						case 6:			//Turn lights on
							if (Taxi.getLightsOn() || Taxi.getEngineOn() == false)
							{
								if (Taxi.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								Taxi.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned the lights on" << endl;
							}
							system("pause");
							break;

						case 7:			//Turn lights off
							if (Taxi.getEngineOn() == false || Taxi.getLightsOn() == false)
							{
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (Taxi.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								Taxi.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned off the lights" << endl;
							}
							system("pause");
							break;
						case 8:			//Add Passengers
							if (Taxi.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (Taxi.getNumPass() + numPass > 4)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 4" << endl;
								}
								else {
									Taxi.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User added " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;

						case 9:			//Kick Passengers
							if (Taxi.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > Taxi.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									Taxi.kickPass(numPass);
									cout << "Passengers kicked out successfully" << endl;
									fout << "User kicked out " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:			//Check mileage
							cout << "You car's mileage is: " << Taxi.getMileage() << " miles" << endl;
							system("pause");
							break;
						case 11:			//Set coefficient of friction
							cout << "What is the current coefficient of friction" << endl;
							cin >> coFriction;
							if (coFriction < 0)
							{
								cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
							}
							else if (coFriction > 1)
							{
								cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
							}
							else {
								Taxi.setCoFriction(coFriction);
								cout << "Coefficient is updated successfully" << endl;
								fout << "Coefficient is updated to " << coFriction << endl;
							}
							system("pause");
							break;
						case 12:			//Check friction
							cout << "Current friction on your car is: " << Taxi.getFriction();
							system("pause");
							break;
						case 13:			//Turn air on
							if (Taxi.getAirOn() || Taxi.getEngineOn() == false || Taxi.getSunroofOpen())
							{
								if (Taxi.getAirOn())
								{
									cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
								}
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn air on if engine is off" << endl;
								}
								if (Taxi.getSunroofOpen())
								{
									cout << "Invalid input: Cannot turn air on if sunroof is open" << endl;
								}
							}
							else {
								Taxi.turnAirOn();
								cout << "Air is now on" << endl;
								fout << "User turned on the air" << endl;
							}
							system("pause");
							break;
						case 14:			//Turn air off
							if (Taxi.getAirOn() == false)
							{
								cout << "Invalid input: Cannot turn air off if it is already off" << endl;
							}
							else {
								Taxi.turnAirOff();
								cout << "Air is now off" << endl;
								fout << "User turned off the air" << endl;
							}
							system("pause");
							break;
						case 15:			//Change race car status
							cout << "Type 1 if it IS a race car and type 0 if it IS NOT a race car: ";
							cin >> tempStat;
							if (tempStat < 0 || tempStat > 1)
							{
								cout << "Invalid input: Only accepted values are 0 and 1" << endl;
							}
							else {
								Taxi.setRaceCarStatus(tempStat);
								cout << "Race Car status updated" << endl;
								if (tempStat)
								{
									fout << "Race car status updated to true" << endl;
								}
								else {
									fout << "Race car status updated to false" << endl;
								}
							}
							system("pause");
							break;
						case 16:			//Open sunroof
							if (Taxi.getSunroofOpen() || Taxi.getEngineOn() == false || Taxi.getAirOn())
							{
								if (Taxi.getSunroofOpen())
								{
									cout << "Invalid input: Cannot open sunroof if it is already open" << endl;
								}
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot open sunroof if engine is off" << endl;
								}
								if (Taxi.getAirOn())
								{
									cout << "Invalid input: Cannot open sunroof if A/C is on" << endl;
								}
							}
							else {
								Taxi.setSunRoofStatus(true);
								cout << "Sunroof is now open" << endl;
								fout << "User opened sunroof" << endl;
							}
							system("pause");
							break;
						case 17:			//Close sunroof
							if (Taxi.getSunroofOpen() == false || Taxi.getEngineOn() == false)
							{
								if (Taxi.getSunroofOpen() == false)
								{
									cout << "Invalid input: Cannot close sunroof if it is already closed" << endl;
								}
								if (Taxi.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot close sunroof if engine is off" << endl;
								}
							}
							else {
								Taxi.setSunRoofStatus(false);
								cout << "Sunroof is now closed" << endl;
								fout << "User closed sunroof" << endl;
							}
							system("pause");
							break;
						case 18:			//Turn right
							if (Taxi.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Taxi.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (Taxi.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								Taxi.turnRight(turnsTripTwo);
								cout << "You have turned right" << endl;
								fout << "User turned right" << endl;
								Taxi.addMileage(10);
							}
							system("pause");
							break;
						case 19:			//Turn left
							if (Taxi.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Taxi.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (Taxi.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								Taxi.turnLeft(turnsTripTwo);
								cout << "You have turned left" << endl;
								fout << "User turned left" << endl;
								Taxi.addMileage(10);
							}
							system("pause");
							break;
						case 20:			//Accelerate
							if (Taxi.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (Taxi.getSpeed() == 45)
							{
								cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
							}
							else {
								Taxi.accelerate();
								cout << "Speed is now increased to " << Taxi.getSpeed() << endl;
								fout << "User accelerated vehicle to " << Taxi.getSpeed() << endl;
								Taxi.addMileage(10);
							}
							system("pause");
							break;
						case 21:			//Decelerate
							if (Taxi.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (Taxi.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
							}
							else {
								Taxi.decelerate();
								cout << "Speed is now decreased to " << Taxi.getSpeed() << endl;
								fout << "User decelerated vehicle to " << Taxi.getSpeed() << endl;
								Taxi.addMileage(10);
							}
							system("pause");
							break;
						case 22:			//Return to last location
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripOne.at(i) == 1)
								{
									turnsTripOne[i] = 3;
								}
								else {
									turnsTripOne[i] = 1;
								}
							}
							reverse(turnsTripTwo.begin(), turnsTripTwo.end());
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripTwo.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left" << endl;
									Taxi.addMileage(10);
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right " << endl;
									Taxi.addMileage(10);
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							cout << "You are at home now" << endl;
							fout << "User has returned to last location" << endl;
							system("pause");
							break;
						case 23:			//Reached destination
							if (Taxi.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
								cin >> userLocation;
								if (userLocation == 2)
								{
									location.push_back(2);
								}
								else if (userLocation == 3)
								{
									location.push_back(3);
								}
								else {
									cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
								}
								fout << "User has ended this trip" << endl;
								Taxi.addMileage(20);
							}
							system("pause");
							break;
						case 0:			//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						};
						//_________________________________________Menu_________________________________________
					}
					if (userInput == 0)
					{
						break;
					}
				} while (userInput != 23);
			}


			else if (userMenu == 2)					//If user selected Truck
			{
				
				vehicle.push_back(3);
				do {
					system("CLS");
					cout << "You are driving Truck" << endl;
					printTruckMenu();
					cin >> userInput;

					if (location.at(location.size() - 2) == 1)				//Tank Truck
					{
						//_________________________________________Menu_________________________________________
						switch (userInput) {
						case 1:			//Set age of Truck
							cout << "What is the age of truck now?: ";
							cin >> userSelection;
							if (userSelection < TankTruck.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == TankTruck.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								TankTruck.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 2:			//Set price of Truck
							cout << "What is the price of your truck now?: ";
							cin >> userSelection;
							if (userSelection > TankTruck.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == TankTruck.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								TankTruck.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:			//Check balance of user
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;
							}
							system("pause");
							break;
						case 4:			//Turn engine on
							if (TankTruck.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								TankTruck.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turned on the engine" << endl;
							}
							system("pause");
							break;
						case 5:			//Turn engine off
							if (TankTruck.getEngineOn() == false || TankTruck.getSpeed() != 0 || TankTruck.getAirOn() == true || TankTruck.getLightsOn())
							{
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (TankTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (TankTruck.getAirOn() == true)
								{
									cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
								}
								if (TankTruck.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								TankTruck.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned off the engine" << endl;
							}
							system("pause");
							break;
						case 6:			//Turn lights on
							if (TankTruck.getLightsOn() || TankTruck.getEngineOn() == false)
							{
								if (TankTruck.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								TankTruck.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned on the lights" << endl;
							}
							system("pause");
							break;

						case 7:			//Turn lights off
							if (TankTruck.getEngineOn() == false || TankTruck.getLightsOn() == false)
							{
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (TankTruck.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								TankTruck.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned ooff the ligths" << endl;
							}
							system("pause");
							break;
						case 8:			//Add Passengers
							if (TankTruck.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (TankTruck.getNumPass() + numPass > 1)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 1" << endl;
								}
								else {
									TankTruck.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User added " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;

						case 9:			//Kick Passengers
							if (TankTruck.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > TankTruck.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									TankTruck.kickPass(numPass);
									cout << "Passengers kicked out successfully" << endl;
									fout << "User kicked out " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:			//Check mileage
							cout << "You car's mileage is: " << TankTruck.getMileage() << " miles" << endl;
							system("pause");
							break;
						case 11:			//Set coefficient of friction
							cout << "What is the current coefficient of friction" << endl;
							cin >> coFriction;
							if (coFriction < 0)
							{
								cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
							}
							else if (coFriction > 1)
							{
								cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
							}
							else {
								TankTruck.setCoFriction(coFriction);
								cout << "Coefficient is updated successfully" << endl;
								fout << "Coefficient is updated to " << coFriction << endl;
							}
							system("pause");
							break;
						case 12:			//Check friction
							cout << "Current friction on your car is: " << TankTruck.getFriction();
							system("pause");
							break;
						case 13:			//Turn air on
							if (TankTruck.getAirOn() || TankTruck.getEngineOn() == false)
							{
								if (TankTruck.getAirOn())
								{
									cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
								}
								if (TankTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn air on if engine is off" << endl;
								}
							}
							else {
								TankTruck.turnAirOn();
								cout << "Air is now on" << endl;
								fout << "User turned on the air" << endl;
							}
							system("pause");
							break;
						case 14:			//Turn air off
							if (TankTruck.getAirOn() == false)
							{
								cout << "Invalid input: Cannot turn air off if it is already off" << endl;
							}
							else {
								TankTruck.turnAirOff();
								cout << "Air is now off" << endl;
								fout << "User turned off the air" << endl;
							}
							system("pause");
							break;
						case 15:			//Change diesel type status
							cout << "Type 1 if it IS a diesel type and type 0 if it IS NOT a diesel type: ";
							cin >> tempStat;
							if (tempStat < 0 || tempStat > 1)
							{
								cout << "Invalid input: Only accepted values are 0 and 1" << endl;
							}
							else {
								TankTruck.setDieselTypeStatus(tempStat);
								cout << "Diesel type status updated" << endl;
								if (tempStat)
								{
									fout << "Diesel type status updated to true" << endl;
								}
								else {
									fout << "Diesel type status updated to false" << endl;
								}
							}
							system("pause");
							break;
						case 16:			//Load cargo
							cout << "How much do you want to load?: " << endl;
							cin >> userCapacity;
							if (userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || TankTruck.getCapacity() + userCapacity > 100 || TankTruck.getSpeed() != 0)
							{
								if (100 - TankTruck.getCapacity() < userCapacity)
								{
									cout << "Invalid input: Cannot add to end up with more than 100 cargo" << endl;
								}
								if (userCapacity < 0)
								{
									cout << "Invalid input: Cannot add 0 cargo" << endl;
								}
								if (userCapacity == 0)
								{
									cout << "Invalid input: Cannot add 0 cargo" << endl;
								}
								if (TankTruck.getCapacity() > 100)
								{
									cout << "Invalid input: Cannot add more than 100 cargo" << endl;
								}
								if (TankTruck.getCapacity() + userCapacity > 100)
								{
									cout << "Invalid input: Cannot add cargo to have total of more than 100 cargo" << endl;
								}
								if (TankTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot add cargo while vehicle is moving" << endl;
								}
							}
							else {
								TankTruck.loadCargo(userCapacity);
								cout << "Cargo loaded successfully" << endl;
								fout << "User added " << userCapacity << " cargo" << endl;
							}
							system("pause");
							break;
						case 17:			//Unload Cargo
							cout << "How much do you want to unload?: " << endl;
							cin >> userCapacity;
							if (userCapacity > TankTruck.getCapacity() || userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || TankTruck.getSpeed() != 0)
							{
								if (userCapacity > TankTruck.getCapacity())
								{
									cout << "Invalid input: Cannot unload to end up with negative cargo" << endl;
								}
								if (userCapacity < 0)
								{
									cout << "Invalid input: Cannot have a negative cargo" << endl;
								}
								if (userCapacity == 0)
								{
									cout << "Invalid input: Cannot unload 0 cargo" << endl;
								}
								if (userCapacity > 100)
								{
									cout << "Invalid input: Cannot unload more than 100 cargo" << endl;
								}
								if (TankTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot unload cargo while vehicle is moving" << endl;
								}
							}
							else {
								TankTruck.loadCargo(userCapacity);
								cout << "Cargo loaded successfully" << endl;
								fout << "User took out " << userCapacity << " cargo" << endl;
							}
							system("pause");
							break;
						case 18:			//Check cargo
							cout << "Current cargo is " << TankTruck.getCapacity() << endl;
							system("pause");
							break;
						case 19:			//Turn right
							if (TankTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (TankTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (TankTruck.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								TankTruck.turnRight(turnsTripOne);
								cout << "You have turned right" << endl;
								fout << "User turned right " << endl;
								TankTruck.addMileage(10);
							}
							system("pause");
							break;
						case 20:			//Turn left
							if (TankTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (TankTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (TankTruck.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								TankTruck.turnLeft(turnsTripOne);
								cout << "You have turned left" << endl;
								fout << "User turned left" << endl;
								TankTruck.addMileage(10);
							}
							system("pause");
							break;
						case 21:			//Accelerate
							if (TankTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (TankTruck.getSpeed() == 45)
							{
								cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
							}
							else {
								TankTruck.accelerate();
								cout << "Speed is now increased to " << TankTruck.getSpeed() << endl;
								fout << "User accelerated vehicle to " << TankTruck.getSpeed() << endl;
								TankTruck.addMileage(10);
							}
							system("pause");
							break;
						case 22:			//Decelerate
							if (TankTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (TankTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
							}
							else {
								TankTruck.decelerate();
								cout << "Speed is now decreased to " << TankTruck.getSpeed() << endl;
								fout << "User decelerated vehicle to " << TankTruck.getSpeed() << endl;
							}
							system("pause");
							break;
						case 23:			//Return to last location
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripTwo.at(i) == 1)
								{
									turnsTripTwo[i] = 3;
								}
								else {
									turnsTripTwo[i] = 1;
								}
							}
							reverse(turnsTripTwo.begin(), turnsTripTwo.end());
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripTwo.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left" << endl;
									TankTruck.addMileage(10);
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right" << endl;
									TankTruck.addMileage(10);
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							cout << "You are at home now" << endl;
							fout << "User has returned to last location" << endl;
							TankTruck.addMileage(20);

							system("pause");
							break;
						case 24:			//Reached destination
							if (TankTruck.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
								cin >> userLocation;
								if (userLocation == 2)
								{
									location.push_back(2);
								}
								else if (userLocation == 3)
								{
									location.push_back(3);
								}
								else {
									cout << "Invalid input: Only values acceptable are 2 or 3" << endl;
								}
								fout << "User has reached destionation" << endl;
								TankTruck.addMileage(20);
							}
							system("pause");
							break;
						case 0:			//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						};
						//_________________________________________Menu_________________________________________
					}
					else													//HaulTruck
					{
						//_________________________________________Menu_________________________________________
						switch (userInput) {
						case 1:			//Set age of Truck
							cout << "What is the age of truck now?: ";
							cin >> userSelection;
							if (userSelection < HaulTruck.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == HaulTruck.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								HaulTruck.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 2:			//Set price of Truck
							cout << "What is the price of your truck now?: ";
							cin >> userSelection;
							if (userSelection > HaulTruck.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == HaulTruck.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								HaulTruck.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price is stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:			//Check balance of user
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;
							}
							system("pause");
							break;
						case 4:			//Turn engine on
							if (HaulTruck.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								HaulTruck.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turned on the engine" << endl;
							}
							system("pause");
							break;
						case 5:			//Turn engine off
							if (HaulTruck.getEngineOn() == false || HaulTruck.getSpeed() != 0 || HaulTruck.getAirOn() == true || HaulTruck.getLightsOn())
							{
								if (HaulTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (HaulTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (HaulTruck.getAirOn() == true)
								{
									cout << "Invalid input: Cannot turn engine off while A/C is on" << endl;
								}
								if (HaulTruck.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								HaulTruck.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned off the engine" << endl;
							}
							system("pause");
							break;
						case 6:			//Turn lights on
							if (HaulTruck.getLightsOn() || HaulTruck.getEngineOn() == false)
							{
								if (HaulTruck.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (HaulTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								HaulTruck.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned on the lights" << endl;
							}
							system("pause");
							break;

						case 7:			//Turn lights off
							if (HaulTruck.getEngineOn() == false || HaulTruck.getLightsOn() == false)
							{
								if (HaulTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (HaulTruck.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								HaulTruck.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned off the lights" << endl;
							}
							system("pause");
							break;
						case 8:			//Add Passengers
							if (HaulTruck.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (HaulTruck.getNumPass() + numPass > 8)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 1" << endl;
								}
								else {
									HaulTruck.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User added " << numPass << " passengers " << endl;
								}
							}
							system("pause");
							break;

						case 9:			//Kick Passengers
							if (HaulTruck.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will die" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > HaulTruck.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									HaulTruck.kickPass(numPass);
									cout << "Passengers kicked out successfully" << endl;
									fout << "User kicked out " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:			//Check mileage
							cout << "You car's mileage is: " << HaulTruck.getMileage() << " miles" << endl;
							system("pause");
							break;
						case 11:			//Set coefficient of friction
							cout << "What is the current coefficient of friction" << endl;
							cin >> coFriction;
							if (coFriction < 0)
							{
								cout << "Invalid input: Cannot have negative coefficient of friction" << endl;
							}
							else if (coFriction > 1)
							{
								cout << "Invalid input: Cannot have coefficient of friction more than 1" << endl;
							}
							else {
								HaulTruck.setCoFriction(coFriction);
								cout << "Coefficient is updated successfully" << endl;
								fout << "Coefficeient is updated to " << coFriction << endl;
							}
							system("pause");
							break;
						case 12:			//Check friction
							cout << "Current friction on your car is: " << HaulTruck.getFriction();
							system("pause");
							break;
						case 13:			//Turn air on
							if (HaulTruck.getAirOn() || HaulTruck.getEngineOn() == false)
							{
								if (HaulTruck.getAirOn())
								{
									cout << "Invalid input: Cannot turn A/C on while it is already on" << endl;
								}
								if (HaulTruck.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn air on if engine is off" << endl;
								}
							}
							else {
								HaulTruck.turnAirOn();
								cout << "Air is now on" << endl;
								fout << "User turned on the air" << endl;
							}
							system("pause");
							break;
						case 14:			//Turn air off
							if (HaulTruck.getAirOn() == false)
							{
								cout << "Invalid input: Cannot turn air off if it is already off" << endl;
							}
							else {
								HaulTruck.turnAirOff();
								cout << "Air is now off" << endl;
								fout << "User turned off the air" << endl;
							}
							system("pause");
							break;
						case 15:			//Change diesel type status
							cout << "Type 1 if it IS a diesel type and type 0 if it IS NOT a diesel type: ";
							cin >> tempStat;
							if (tempStat < 0 || tempStat > 1)
							{
								cout << "Invalid input: Only accepted values are 0 and 1" << endl;
							}
							else {
								HaulTruck.setDieselTypeStatus(tempStat);
								cout << "Diesel type status updated" << endl;
								if (tempStat)
								{
									fout << "Diesel type status updated to true" << endl;
								}
								else {
									fout << "Diesel type status updated to false " << endl;
								}
							}
							system("pause");
							break;
						case 16:			//Load cargo
							cout << "How much do you want to load?: " << endl;
							cin >> userCapacity;
							if (userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || HaulTruck.getCapacity() + userCapacity > 100 || HaulTruck.getSpeed() != 0)
							{
								if (100 - HaulTruck.getCapacity() < userCapacity)
								{
									cout << "Invalid input: Cannot add to end up with more than 100 cargo" << endl;
								}
								if (userCapacity < 0)
								{
									cout << "Invalid input: Cannot add 0 cargo" << endl;
								}
								if (userCapacity == 0)
								{
									cout << "Invalid input: Cannot add 0 cargo" << endl;
								}
								if (HaulTruck.getCapacity() > 100)
								{
									cout << "Invalid input: Cannot add more than 100 cargo" << endl;
								}
								if (HaulTruck.getCapacity() + userCapacity > 100)
								{
									cout << "Invalid input: Cannot add cargo to have total of more than 100 cargo" << endl;
								}
								if (HaulTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot add cargo while vehicle is moving" << endl;
								}
							}
							else {
								HaulTruck.loadCargo(userCapacity);
								cout << "Cargo loaded successfully" << endl;
								fout << "User added " << userCapacity << " cargo" << endl;
							}
							system("pause");
							break;
						case 17:			//Unload Cargo
							cout << "How much do you want to unload?: " << endl;
							cin >> userCapacity;
							if (userCapacity > HaulTruck.getCapacity() || userCapacity < 0 || userCapacity == 0 || userCapacity > 100 || HaulTruck.getSpeed() != 0)
							{
								if (userCapacity > HaulTruck.getCapacity())
								{
									cout << "Invalid input: Cannot unload to end up with negative cargo" << endl;
								}
								if (userCapacity < 0)
								{
									cout << "Invalid input: Cannot have a negative cargo" << endl;
								}
								if (userCapacity == 0)
								{
									cout << "Invalid input: Cannot unload 0 cargo" << endl;
								}
								if (userCapacity > 100)
								{
									cout << "Invalid input: Cannot unload more than 100 cargo" << endl;
								}
								if (HaulTruck.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot unload cargo while vehicle is moving" << endl;
								}
							}
							else {
								HaulTruck.loadCargo(userCapacity);
								cout << "Cargo unloaded successfully" << endl;
								fout << "User took out " << userCapacity << " cargo" << endl;
							}
							system("pause");
							break;
						case 18:			//Check cargo
							cout << "Current cargo is " << HaulTruck.getCapacity() << endl;
							system("pause");
							break;
						case 19:			//Turn right
							if (HaulTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (HaulTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (HaulTruck.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								HaulTruck.turnRight(turnsTripOne);
								cout << "You have turned right" << endl;
								fout << "User turned right " << endl;
								HaulTruck.addMileage(10);
							}
							system("pause");
							break;
						case 20:			//Turn left
							if (HaulTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (HaulTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else if (HaulTruck.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot turn if speed is greater than 10" << endl;
							}
							else {
								HaulTruck.turnLeft(turnsTripOne);
								cout << "You have turned left" << endl;
								fout << "User turned left " << endl;
								HaulTruck.addMileage(10);
							}
							system("pause");
							break;
						case 21:			//Accelerate
							if (HaulTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (HaulTruck.getSpeed() == 45)
							{
								cout << "Invalid input: Cannot accelerate to more than 45 mph" << endl;
							}
							else {
								HaulTruck.accelerate();
								cout << "Speed is now increased to " << HaulTruck.getSpeed() << endl;
								fout << "User increased speed to " << HaulTruck.getSpeed() << endl;
								HaulTruck.addMileage(10);
							}
							system("pause");
							break;
						case 22:			//Decelerate
							if (HaulTruck.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (HaulTruck.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decelerate to less than 0 mph" << endl;
							}
							else {
								HaulTruck.decelerate();
								cout << "Speed is now decreased to " << HaulTruck.getSpeed() << endl;
								fout << "Speed is decreased to " << HaulTruck.getSpeed() << endl;
								HaulTruck.addMileage(10);
							}
							system("pause");
							break;
						case 23:			//Return to last location
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripTwo.at(i) == 1)
								{
									turnsTripTwo[i] = 3;
								}
								else {
									turnsTripTwo[i] = 1;
								}
							}
							reverse(turnsTripTwo.begin(), turnsTripTwo.end());
							for (int i = 0; i < turnsTripTwo.size(); i++)
							{
								if (turnsTripTwo.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left " << endl;
									HaulTruck.addMileage(10);
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right " << endl;
									HaulTruck.addMileage(10);
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							cout << "You are at home now" << endl;
							fout << "User has returned to last lcoation" << endl;
							HaulTruck.addMileage(20);
							system("pause");
							break;
						case 24:			//Reached destination
							if (HaulTruck.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								cout << "What location is this? Type 2 for Airport. Type 3 for Marina: ";
								cin >> userLocation;
								if (userLocation == 2)
								{
									location.push_back(2);
								}
								else if (userLocation == 3)
								{
									location.push_back(3);
								}
								else {
									cout << "Invalid input: Only values acceptable are 2 or 3" << endl;

								}
								fout << "User has ended this trip" << endl;
								HaulTruck.addMileage(20);
							}
							system("pause");
							break;
						case 0:			//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						};
						//_________________________________________Menu_________________________________________
					}
					if (userInput == 0)
					{
						break;
					}
				} while (userInput != 24);
			}


			else if (userMenu == 3)					//If user selected Boat
			{
				
				vehicle.push_back(3);
				do {
					system("CLS");
					cout << "You are driving Boat" << endl;
					printBoatMenu();
					cin >> userInput;

					if (location.at(location.size() - 2) == 1)				//Hovercraft
					{
						//_________________________________________Menu_________________________________________
						switch (userInput)
						{
						case 1:						//Set age
							cout << "What is the age of plane now?: ";
							cin >> userSelection;
							if (userSelection < Hovercraft.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == Hovercraft.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								Hovercraft.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 2:						//Set price
							cout << "What is the price of your plane now?: ";
							cin >> userSelection;
							if (userSelection > Hovercraft.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == Hovercraft.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								Hovercraft.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:						//Check balance
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;		//Don't change
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;	//Don't change
							}
							system("pause");
							break;
						case 4:						//Turn engine on
							if (Hovercraft.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								Hovercraft.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turns on the engine" << endl;
							}
							system("pause");
							break;
						case 5:						//Turn engine off
							if (Hovercraft.getEngineOn() == false || Hovercraft.getSpeed() != 0 || Hovercraft.getLightsOn() || Hovercraft.getLaunch())
							{
								if (Hovercraft.getLaunch())
								{
									cout << "Invalid input: Cannot turn engine off while boat is not docked" << endl;
								}
								if (Hovercraft.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (Hovercraft.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (Hovercraft.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								Hovercraft.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned off the engine " << endl;
							}
							system("pause");
							break;
						case 6:						//Turn lights on
							if (Hovercraft.getLightsOn() || Hovercraft.getEngineOn() == false)
							{
								if (Hovercraft.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (Hovercraft.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								Hovercraft.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned on the lights " << endl;
							}
							system("pause");
							break;
						case 7:						//Turn lights off
							if (Hovercraft.getEngineOn() == false || Hovercraft.getLightsOn() == false)
							{
								if (Hovercraft.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (Hovercraft.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								Hovercraft.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned off the ligths " << endl;
							}
							system("pause");
							break;
						case 8:						//Add passengers
							if (Hovercraft.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (Hovercraft.getNumPass() + numPass > 8)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 8" << endl;
								}
								else {
									Hovercraft.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User adds " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 9:						//Kick passengers
							if (Hovercraft.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will drown" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > Hovercraft.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									Hovercraft.kickPass(numPass);
									cout << "Passengers kicked out successfully" << endl;
									fout << "User kicked out " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:					//Check propeller level
							cout << "Your propeller level is " << Hovercraft.getPropellerLevel() << endl;
							system("pause");
							break;
						case 11:					//Change propeller level
							if (Hovercraft.getEngineOn() == false)
							{
								cout << "Invalid input. Cannot change propeller level is engine is off" << endl;
							}
							else
							{
								cout << "What level would you like to set the propeller to?: ";
								cin >> userPropeller;
								if (userPropeller == Hovercraft.getPropellerLevel())
								{
									cout << "Invalid input. Cannot set propeller level to be the same as before" << endl;
								}
								else if (userPropeller == 0)
								{
									if (Hovercraft.getDock())
									{
										Hovercraft.setPropellerLevel(0);
										cout << "Propeller level is set successfully to 0. Boat will no longer move" << endl;
										fout << "User changed propeller level to " << userPropeller << endl;
									}
									else {
										cout << "Invalid input: Cannot set propeller level to 0 while boat is not docked" << endl;
									}
								}
								else if (userPropeller > 5)
								{
									cout << "Invalid input: Cannot set propeller level to be more than 5. That would be too deep" << endl;
								}
								else if (userPropeller < 0)
								{
									cout << "Invalid input: Cannot set propeller level to a negative value" << endl;
								}
								else {
									Hovercraft.setPropellerLevel(userPropeller);
									cout << "Propeller level set successfully" << endl;
									fout << "User changed propeller level to " << userPropeller << endl;
								}
							}
							system("pause");
							break;
						case 12:					//Dock the boat
							if (Hovercraft.getDock())
							{
								cout << "Invalid input: Cannot dock the boat if it is already docked" << endl;
							}
							else if (Hovercraft.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot dock the boat if speed is more than 10 mph" << endl;
							}
							else {
								Hovercraft.setDock(true);
								Hovercraft.setLaunch(false);
								cout << "Boat is now docked" << endl;
								fout << "User docked the boat" << endl;
							}
							system("pause");
							break;
						case 13:					//Launch the boat
							if (Hovercraft.getLaunch())
							{
								cout << "Invalid input: Cannot launch the boat if it is already launched" << endl;
							}
							else if (Hovercraft.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot launch the boat if speed is 0 mph" << endl;
							}
							else {
								Hovercraft.setLaunch(true);
								Hovercraft.setDock(false);
								cout << "Boat is now launched" << endl;
								fout << "User launched the boat" << endl;
							}
							system("pause");
							break;
						case 14:					//Turn right
							if (Hovercraft.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Hovercraft.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else {
								Hovercraft.turnRight(turnsTripThree);
								cout << "You have turned right" << endl;
								fout << "User turned right" << endl;
							}
							system("pause");
							break;
						case 15:					//Turn left
							if (Hovercraft.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Hovercraft.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else {
								Hovercraft.turnLeft(turnsTripThree);
								cout << "You have turned left" << endl;
								fout << "User turned left" << endl;
							}
							system("pause");
							break;
						case 16:					//Accelerate
							if (Hovercraft.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (Hovercraft.getPropellerLevel() == 0)
							{
								cout << "Invalid input: Cannot accelerate if propeller level is 0" << endl;
							}
							else
							{
								if (Hovercraft.getDock() && Hovercraft.getSpeed() == 10)
								{
									cout << "Invalid input: Cannot accelerate to more than 10 mph while boat is docked" << endl;
								}
								else if (Hovercraft.getSpeed() == 45)
								{
									cout << "Invalid input: Cannot go above 45 mph" << endl;
								}
								else {
									Hovercraft.accelerate();
									cout << "Speed is now increased to " << Hovercraft.getSpeed() << endl;
									fout << "User increased speed to " << Hovercraft.getSpeed() << endl;
								}
							}
							system("pause");
							break;
						case 17:					//Decelerate
							if (Hovercraft.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (Hovercraft.getPropellerLevel() == 0)
							{
								cout << "Invalid input: Cannot decelerate if propeller level is 0" << endl;
							}
							else if (Hovercraft.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decrease speed to a negative value" << endl;
							}
							else {
								Hovercraft.decelerate();
								cout << "Speed is now decreased to " << Hovercraft.getSpeed() << endl;
								fout << "User decreased speed to " << Hovercraft.getSpeed() << endl;
							}
							system("pause");
							break;
						case 18:					//Return to last location
							for (int i = 0; i < turnsTripThree.size(); i++)
							{
								if (turnsTripThree.at(i) == 1)
								{
									turnsTripThree[i] = 3;
								}
								else {
									turnsTripThree[i] = 1;
								}
							}
							reverse(turnsTripThree.begin(), turnsTripThree.end());
							for (int i = 0; i < turnsTripThree.size(); i++)
							{
								if (turnsTripThree.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left" << endl;
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right" << endl;
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							fout << "User has returend to last location" << endl;
							system("pause");
							break;
						case 19:					//Reached destination
							if (Hovercraft.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								location.push_back(3);
								fout << "User has ended this trip" << endl;
							}
							system("pause");
							break;
						case 0:						//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						}
						//_________________________________________Menu_________________________________________
					}
					else													//Yacht
					{
						//_________________________________________Menu_________________________________________
						switch (userInput)
						{
						case 1:						//Set age
							cout << "What is the age of plane now?: ";
							cin >> userSelection;
							if (userSelection < Yacht.getAge())
							{
								cout << "Invalid input. Age cannot be less than last one" << endl;
								system("pause");
							}
							else if (userSelection == Yacht.getAge())
							{
								cout << "The age you entered is the same age stored earlier" << endl;
								system("pause");
							}
							else {
								Yacht.setAge(userSelection);
								cout << "Age stored successfully" << endl;
								fout << "Age stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 2:						//Set price
							cout << "What is the price of your plane now?: ";
							cin >> userSelection;
							if (userSelection > Yacht.getPrice())
							{
								cout << "Invalid input. Price cannot be more than last one" << endl;
								system("pause");
							}
							else if (userSelection == Yacht.getPrice())
							{
								cout << "The price you enteres it the same price stored earlier" << endl;
								system("pause");
							}
							else if (userSelection < 0)
							{
								cout << "Invalid input: Price cannot be negative" << endl;
								system("Pause");
							}
							else {
								Yacht.setPrice(userSelection);
								cout << "Price stored successfully" << endl;
								fout << "Price stored as " << userSelection << endl;
								system("pause");
							}
							break;
						case 3:						//Check balance
							if (vehicle.at(0) == 1)
							{
								cout << "Your balance is " << Lambo.getBalance() << endl;		//Don't change
							}
							else if (vehicle.at(0) == 4)
							{
								cout << "Your balance is " << TankTruck.getBalance() << endl;	//Don't change
							}
							system("pause");
							break;
						case 4:						//Turn engine on
							if (Yacht.getEngineOn())
							{
								cout << "Invalid input: Cannot turn engine on while it is already on" << endl;
							}
							else {
								Yacht.turnEngineOn();
								cout << "Engine is now on" << endl;
								fout << "User turned on the engine" << endl;
							}
							system("pause");
							break;
						case 5:						//Turn engine off
							if (Yacht.getEngineOn() == false || Yacht.getSpeed() != 0 || Yacht.getLightsOn() || Yacht.getLaunch())
							{
								if (Yacht.getLaunch())
								{
									cout << "Invalid input: Cannot turn engine off while boat is not docked" << endl;
								}
								if (Yacht.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn engine off while it is already off" << endl;
								}
								if (Yacht.getSpeed() != 0)
								{
									cout << "Invalid input: Cannot turn engine off while vehicle is moving" << endl;
								}
								if (Yacht.getLightsOn())
								{
									cout << "Invalid input: Cannot turn engine off while lights are on" << endl;
								}
							}
							else {
								Yacht.turnEngineOff();
								cout << "Engine is now off" << endl;
								fout << "User turned off the engine" << endl;
							}
							system("pause");
							break;
						case 6:						//Turn lights on
							if (Yacht.getLightsOn() || Yacht.getEngineOn() == false)
							{
								if (Yacht.getLightsOn())
								{
									cout << "Invalid input: Cannot turn lights on if they are already on" << endl;
								}
								if (Yacht.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn lights on if engine is off" << endl;
								}
							}
							else {
								Yacht.turnLightsOn();
								cout << "Lights are now on" << endl;
								fout << "User turned on the lights" << endl;
							}
							system("pause");
							break;
						case 7:						//Turn lights off
							if (Yacht.getEngineOn() == false || Yacht.getLightsOn() == false)
							{
								if (Yacht.getEngineOn() == false)
								{
									cout << "Invalid input: Cannot turn turn lights off while engine is off" << endl;
								}
								if (Yacht.getLightsOn() == false)
								{
									cout << "Invalid input: Cannot turn lights off while they are already off" << endl;
								}
							}
							else {
								Yacht.turnLightsOff();
								cout << "Lights are now off" << endl;
								fout << "User turned off the lights" << endl;
							}
							system("pause");
							break;
						case 8:						//Add passengers
							if (Yacht.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot add passengers while vehicle is moving" << endl;
							}
							else {
								cout << "How many passengers do you want to add? ";
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot add 0 passengers" << endl;
								}
								else if (Yacht.getNumPass() + numPass > 8)
								{
									cout << "Invlalid input: Cannot add passengers to have total of more than 8" << endl;
								}
								else {
									Yacht.addPass(numPass);
									cout << "Passengers added successfully" << endl;
									fout << "User adds " << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 9:						//Kick passengers
							if (Yacht.getSpeed() != 0)
							{
								cout << "Invalid input: Cannot kick passengers while vehicle is moving\nThey will drown" << endl;
							}
							else {
								cout << "How many passengers do you want to kick out" << endl;
								cin >> numPass;
								if (numPass == 0)
								{
									cout << "Invalid input: Cannot kick 0 passengers" << endl;
								}
								else if (numPass > Yacht.getNumPass())
								{
									cout << "Invalid input: Cannot kick more passengers than there are" << endl;
								}
								else {
									Yacht.kickPass(numPass);
									cout << "Passengers kicked out successfully" << endl;
									fout << "User kicks out	" << numPass << " passengers" << endl;
								}
							}
							system("pause");
							break;
						case 10:					//Check propeller level
							cout << "Your propeller level is " << Yacht.getPropellerLevel() << endl;
							system("pause");
							break;
						case 11:					//Change propeller level
							if (Yacht.getEngineOn() == false)
							{
								cout << "Invalid input. Cannot change propeller level is engine is off" << endl;
							}
							else
							{
								cout << "What level would you like to set the propeller to?: ";
								cin >> userPropeller;
								if (userPropeller == Yacht.getPropellerLevel())
								{
									cout << "Invalid input. Cannot set propeller level to be the same as before" << endl;
								}
								else if (userPropeller == 0)
								{
									if (Yacht.getDock())
									{
										Yacht.setPropellerLevel(0);
										cout << "Propeller level is set successfully to 0. Boat will no longer move" << endl;
										fout << "User changed propeller level to " << userPropeller << endl;
									}
									else {
										cout << "Invalid input: Cannot set propeller level to 0 while boat is not docked" << endl;
									}
								}
								else if (userPropeller < 0)
								{
									cout << "Invalid input: Cannot set propeller level to a negative value" << endl;
								}
								else if (userPropeller > 5)
								{
									cout << "Invalid input: Cannot set propeller level to be more than 5. That would be too deep" << endl;
								}
								else {
									Yacht.setPropellerLevel(userPropeller);
									cout << "Propeller level set successfully" << endl;
									fout << "User changed propeller level to " << userPropeller << endl;
								}
							}
							system("pause");
							break;
						case 12:					//Dock the boat
							if (Yacht.getDock())
							{
								cout << "Invalid input: Cannot dock the boat if it is already docked" << endl;
							}
							else if (Yacht.getSpeed() > 10)
							{
								cout << "Invalid input: Cannot dock the boat if speed is more than 10 mph" << endl;
							}
							else {
								Yacht.setDock(true);
								Yacht.setLaunch(false);
								cout << "Boat is now docked" << endl;
								fout << "User docked the boad" << endl;
							}
							system("pause");
							break;
						case 13:					//Launch the boat
							if (Yacht.getLaunch())
							{
								cout << "Invalid input: Cannot launch the boat if it is already launched" << endl;
							}
							else if (Yacht.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot launch the boat if speed is 0 mph" << endl;
							}
							else {
								Yacht.setLaunch(true);
								Yacht.setDock(false);
								cout << "Boat is now launched" << endl;
								fout << "User launcehd the boat" << endl;
							}
							system("pause");
							break;
						case 14:					//Turn right
							if (Yacht.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Yacht.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else {
								Yacht.turnRight(turnsTripThree);
								cout << "You have turned right" << endl;
								fout << "User turned right" << endl;
							}
							system("pause");
							break;
						case 15:					//Turn left
							if (Yacht.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot turn if engine is off" << endl;
							}
							else if (Yacht.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot turn if speed is 0" << endl;
							}
							else {
								Yacht.turnLeft(turnsTripThree);
								cout << "You have turned left" << endl;
								fout << "User turned left" << endl;
							}
							system("pause");
							break;
						case 16:					//Accelerate
							if (Yacht.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot accelerate if engine is off" << endl;
							}
							else if (Hovercraft.getPropellerLevel() == 0)
							{
								cout << "Invalid input: Cannot accelerate if propeller level is 0" << endl;
							}
							else
							{
								if (Yacht.getDock() && Yacht.getSpeed() == 10)
								{
									cout << "Invalid input: Cannot accelerate to more than 10 mph while boat is docked" << endl;
								}
								else if (Yacht.getSpeed() == 45)
								{
									cout << "Invalid input: Cannot go above 45 mph" << endl;
								}
								else {
									Yacht.accelerate();
									cout << "Speed is now increased to " << Yacht.getSpeed() << endl;
									fout << "User increases speed to " << Yacht.getSpeed() << endl;
								}
							}
							system("pause");
							break;
						case 17:					//Decelerate
							if (Yacht.getEngineOn() == false)
							{
								cout << "Invalid input: Cannot decelerate if engine is off" << endl;
							}
							else if (Hovercraft.getPropellerLevel() == 0)
							{
								cout << "Invalid input: Cannot decelerate if propeller level is 0" << endl;
							}
							else if (Yacht.getSpeed() == 0)
							{
								cout << "Invalid input: Cannot decrease speed to a negative value" << endl;
							}
							else {
								Yacht.decelerate();
								cout << "Speed is now decreased to " << Yacht.getSpeed() << endl;
								fout << "User decreases speed to " << Yacht.getSpeed() << endl;
							}
							system("pause");
							break;
						case 18:					//Return to last location
							for (int i = 0; i < turnsTripThree.size(); i++)
							{
								if (turnsTripThree.at(i) == 1)
								{
									turnsTripThree[i] = 3;
								}
								else {
									turnsTripThree[i] = 1;
								}
							}
							reverse(turnsTripThree.begin(), turnsTripThree.end());
							for (int i = 0; i < turnsTripThree.size(); i++)
							{
								if (turnsTripThree.at(i) == 1)
								{
									cout << "Turn left..." << endl;
									fout << "User turned left" << endl;
								}
								else {
									cout << "Turn right..." << endl;
									fout << "User turned right" << endl;
								}
							}
							location[location.size() - 1] = NULL;		//Deletes last location
							fout << "User has returned to last location" << endl;
							system("pause");
							break;
						case 19:					//Reached destination
							if (Yacht.getEngineOn())
							{
								cout << "Invalid input: Cannot get out of vehicle if engine is on" << endl;
							}
							else {
								location.push_back(3);
								fout << "User has ended this trip" << endl;
							}
							system("pause");
							break;
						case 0:						//Quit
							cout << "Sorry to see you go" << endl;
							endOfMenu = true;
							system("pause");
							break;
						}
						//_________________________________________Menu_________________________________________
					}

				} while (userInput != 19);
			}






			system("pause");
		}


		//................... . . . . . . . . . .  .  .  .  .    .    .    .    .    .     .     .     .          .          .          .




		else {
			cout << "Error in location vector..." << endl;
			system("pause");
		}
	} while (endOfMenu == false);













	//Thank you message
	cout << "Bye, human. Will see you next time.....hopefully.....if I don't take over the humanity!" << endl;
	fout.close();


	return 0;
}




void printCarMenu()
{
	cout << "1. Set age" << endl;
	cout << "2. Set price" << endl;
	cout << "3. Check balance" << endl;
	cout << "4. Turn engine on" << endl;
	cout << "5. Turn engine off" << endl;
	cout << "6. Turn lights on" << endl;
	cout << "7. Turn lights off" << endl;
	cout << "8. Add passengers" << endl;
	cout << "9. Kick passengers" << endl;
	cout << "10. Check mileage" << endl;
	cout << "11. Set coefficient of friction" << endl;
	cout << "12. Check friction" << endl;
	cout << "13. Turn air on" << endl;
	cout << "14. Turn air off" << endl;
	cout << "15. Change race car status" << endl;
	cout << "16. Open sunroof" << endl;
	cout << "17. Close sunroof" << endl;
	cout << "18. Turn right" << endl;
	cout << "19. Turn left" << endl;
	cout << "20. Accelerate" << endl;
	cout << "21. Decelerate" << endl;
	cout << "22. Return to last location" << endl;
	cout << "23. Reached destination" << endl;
	cout << "0. Quit" << endl;
}
void printTruckMenu()
{
	cout << "1. Set age" << endl;
	cout << "2. Set price" << endl;
	cout << "3. Check balance" << endl;
	cout << "4. Turn engine on" << endl;
	cout << "5. Turn engine off" << endl;
	cout << "6. Turn lights on" << endl;
	cout << "7. Turn lights off" << endl;
	cout << "8. Add passengers" << endl;
	cout << "9. Kick passengers" << endl;
	cout << "10. Check mileage" << endl;
	cout << "11. Set coefficient of friction" << endl;
	cout << "12. Check friction" << endl;
	cout << "13. Turn air on" << endl;
	cout << "14. Turn air off" << endl;
	cout << "15. Change diesel type status" << endl;
	cout << "16. Load cargo" << endl;
	cout << "17. Unload cargo" << endl;
	cout << "18. Check cargo" << endl;
	cout << "19. Turn right" << endl;
	cout << "20. Turn left" << endl;
	cout << "21. Accelerate" << endl;
	cout << "22. Decelerate" << endl;
	cout << "23. Return to last location" << endl;
	cout << "24. Reached destination" << endl;
	cout << "0. Quit" << endl;
}
void printBoatMenu()
{
	cout << "1. Set age" << endl;
	cout << "2. Set price" << endl;
	cout << "3. Check balance" << endl;
	cout << "4. Turn engine on" << endl;
	cout << "5. Turn engine off" << endl;
	cout << "6. Turn lights on" << endl;
	cout << "7. Turn lights off" << endl;
	cout << "8. Add passengers" << endl;
	cout << "9. Kick passengers" << endl;
	cout << "10. Check propeller level" << endl;
	cout << "11. Change propeller level" << endl;
	cout << "12. Dock the boad" << endl;
	cout << "13. Launch the boat" << endl;
	cout << "14. Turn right" << endl;
	cout << "15. Turn left" << endl;
	cout << "16. Accelerate" << endl;
	cout << "17. Decelerate" << endl;
	cout << "18. Return to last location" << endl;
	cout << "19. Reached destination" << endl;
	cout << "0. Quit" << endl;
}
void printPlaneMenu()
{
	cout << "1. Set age" << endl;
	cout << "2. Set price" << endl;
	cout << "3. Check balance" << endl;
	cout << "4. Turn engine on" << endl;
	cout << "5. Turn engine off" << endl;
	cout << "6. Turn lights on" << endl;
	cout << "7. Turn lights off" << endl;
	cout << "8. Add passengers" << endl;
	cout << "9. Kick passengers" << endl;
	cout << "10. Turn air on" << endl;
	cout << "11. Turn air off" << endl;
	cout << "12. Check altitude" << endl;
	cout << "13. Change altitude" << endl;
	cout << "14. Take off" << endl;
	cout << "15. Land" << endl;
	cout << "16. Turn right" << endl;
	cout << "17. Turn left" << endl;
	cout << "18. Accelerate" << endl;
	cout << "19. Deccelerate" << endl;
	cout << "20. Return to last location" << endl;
	cout << "21. Reached destination" << endl;
	cout << "22. Quit" << endl;
}