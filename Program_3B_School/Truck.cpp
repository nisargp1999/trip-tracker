#include "Truck.h"

Truck::Truck()
{
	dieselTypeStatus = false;
	cargoCapacity = 0;
	maxCapacity = 100;
}
void Truck::setDieselTypeStatus(bool thisStatus)
{
	dieselTypeStatus = thisStatus;
}
void Truck::loadCargo(int thisCargo)
{
	cargoCapacity += thisCargo;
}
void Truck::unloadCargo(int thisCargo)
{
	cargoCapacity -= thisCargo;
}
bool Truck::getDieselTypeStatus()
{
	return dieselTypeStatus;
}
int Truck::getCapacity()
{
	return cargoCapacity;
}