#pragma once
#ifndef TRUCK_H
#define TRUCK_H
#include "Vehicle.h"
#include "LandVehicle.h"

class Truck : public LandVehicle
{
public:
	Truck();
	void setDieselTypeStatus(bool thisStatus);
	void loadCargo(int thisCargo);
	void unloadCargo(int thisCargo);
	bool getDieselTypeStatus();
	int getCapacity();
	int maxCapacity;
private:
	bool dieselTypeStatus;
	int cargoCapacity;
};

#endif // !TRUCK_H
