#include "Vehicle.h"

Vehicle::Vehicle()
{
	age = 0;
	price = 0.0;
	balance = 40000;
	speed = 0;
	engineOn = false;
	lightsOn = false;
	numPass = 0;
}

void Vehicle::setAge(int thisAge)
{
	age = thisAge;
}
void Vehicle::setPrice(float thisPrice)
{
	price = thisPrice;
}
void Vehicle::subtractBalance(float thisBalance)
{
	balance -= thisBalance;
}
void Vehicle::accelerate()
{
	speed += 5;
}
void Vehicle::decelerate()
{
	speed -= 5;
}
void Vehicle::turnEngineOn()
{
	engineOn = true;
}
void Vehicle::turnEngineOff()
{
	engineOn = false;
}
void Vehicle::turnRight(vector<int> &turns)
{
	turns.push_back(3);
}
void Vehicle::turnLeft(vector<int> &turns)
{
	turns.push_back(1);
}
void Vehicle::turnLightsOn()
{
	lightsOn = true;
}
void Vehicle::turnLightsOff()
{
	lightsOn = false;
}
void Vehicle::addPass(int thisPass)
{
	numPass += thisPass;
}
void Vehicle::kickPass(int thisPass)
{
	numPass -= thisPass;
}
float Vehicle::getBalance()
{
	return balance;
}
float Vehicle::getSpeed()
{
	return speed;
}
int Vehicle::getAge()
{
	return age;
}
int Vehicle::getNumPass()
{
	return numPass;
}
float Vehicle::getPrice()
{
	return price;
}
bool Vehicle::getEngineOn()
{
	return engineOn;
}
bool Vehicle::getLightsOn()
{
	return lightsOn;
}