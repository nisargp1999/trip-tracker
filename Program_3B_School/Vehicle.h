#pragma once
#ifndef VECHICLE_H
#define VEHICLE_H
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Vehicle
{
public:
	Vehicle();
	void setAge(int thisAge);
	void setPrice(float thisPrice);
	void subtractBalance(float thisBalance);
	void accelerate();
	void decelerate();
	void turnEngineOn();
	void turnEngineOff();
	void turnRight(vector<int> &turns);
	void turnLeft(vector<int> &turns);
	void turnLightsOn();
	void turnLightsOff();
	void addPass(int thisPass);
	void kickPass(int thisPass);
	float getBalance();
	float getSpeed();
	int getAge();
	int getNumPass();
	float getPrice();
	bool getEngineOn();
	bool getLightsOn();
private:
	int age;
	float price;
	float balance;
	int speed;
	bool engineOn;
	bool lightsOn;
	int numPass;
};

#endif